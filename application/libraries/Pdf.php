<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
 
class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
        $this->Header();
    }
    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'res5_escudo.jpg';
        $this->Image($image_file, 10, 10, 48, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('courier', 'B', 20);
        // Title
        $this->Cell(0, 150, 'Municipalidad de Maipú', 0, false, 'L', 0, '', 0, false, 'C', 'C');
        //OTRO LOGO
        $image_file = K_PATH_IMAGES.'sello_esq2.png';
        $this->Image($image_file, 140,-10, 83, 81, '', '', '', false, 300, '', false, false, 0);
        
        //$image_file = K_PATH_IMAGES.'sello1.png';
        //$this->Image($image_file, 150,1, 54, 32, '', '', '', false, 300, '', false, false, 0);
        
        //$image_file = K_PATH_IMAGES.'sello_copia.png';
        //$this->Image($image_file, 160,7, 40, 24, '', '', '', false, 300, '', false, false, 0);
        
        //$this->Image($image_file, 165, 17, 30, '', 'JPG', '', 'B', false, 300, '', false, false, 0, false, false, false);
    }

}
/* application/libraries/Pdf.php */