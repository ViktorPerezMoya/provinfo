<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notaspedido
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Notaspedido extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('notapedido_model');
        $this->load->helper('form');
    }

    public function index($cont = 0) {
        $this->checarSesion();
        
        /*desactivo el mostrar notas*/
        if($this->session->userdata('show_notape_new')){
            $this->session->set_userdata('show_notape_new', false);
        }
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Notas de Pedido" => "",
        );
        $data['breadcrumb'] = $breadcrumb;
        $data['info'] ="<p>En esta sección usted podrá ver las <strong>Notas de Pedido</strong> que fueron realizadas por el municipio.</p><p> También apareceran en un recuadro azul claro un mensaje con la cantidad de notas de pedido realizadas desde su última sesión.</p><p> El boton azul colocado a la izquierda de cada fila desplegara la lista de items cotizados y no cotizados, mientras que el boton amarillo situado a la derecha accedera a la cotizacion correspondiente.</p><p> Desde esta sección del portal usted podra acceder a la sección de cotización de la misma. También podrá realizar busquedas por año, detalle y tipo de categoría.</p>";
        
        $data['title_head'] = "Notas de Pedido";
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_rules(
                'periodo', 'Numero', 'trim|numeric', array(
            'numeric' => '<p style="color: red;">El %s no es un numero.</p>'
                )
        );
        $this->form_validation->set_rules(
                'detalle', 'Detalle', 'trim', array(
            'alpha_dash' => '<p style="color: red;">El %s no es valido solo se permiten caracters alfanumericos, guion y guion bajo.</p>'
                )
        );

        $this->load->model('itemnota_model');
        $this->load->model('provxrubro_model');
        //traer cuil
        $cuil = $this->session->userdata('cuilprov');
        //traer rubros asignados para usar en filtro
        $misrubros = $this->provxrubro_model->findRubros($cuil);
        //traer notas de pedidos segun rubros como array
        if ($this->form_validation->run() == false) {
            $dataquery = $this->session->flashdata('pbusqueda');
            if ($dataquery !== null && $cont > 0) {
                $this->session->set_flashdata('pbusqueda', $dataquery);
                $notas = $this->notapedido_model->getByRubrosXCuil($cuil, $dataquery);//NOTAS CON FILTRO
            } else {
                $notas = $this->notapedido_model->getByRubrosXCuil($cuil);//NOTAS SIN FILTRO
            }
        } else {
            $numenota = $this->input->post('periodo');
            $detanota = $this->input->post('detalle');
            $detarubro = $this->input->post('selectrubro');
            $dataqry = array("periodo" => $numenota, "detanota" => $detanota, "detarubro" => $detarubro);

            $notas = $this->notapedido_model->getByRubrosXCuil($cuil, $dataqry);//NOTAS CON FILTRO
            $data['dataform'] = $dataqry; //para mantener los datos de busqueda en el formulario 
            $this->session->set_flashdata('pbusqueda', $dataqry);
        }
        //traer item de nota de pedido como array y agregarlo a cada nota de pedido
        $itemsnotas = array();
        foreach ($notas as $n) {
            $items = $this->itemnota_model->getByCodiNope($cuil,$n['PeriInfo'], $n['CodiNope']);
            array_push($itemsnotas, $items);
        }
        //var_dump($itemsnotas);
        $this->load->view('themplate/headprincipal', $data);
        $data['misrubros'] = $misrubros;
        $data['notas'] = $notas;
        $data['itemsnota'] = $itemsnotas;
        $data['arrayjs'] = array('scripts.js');

        //paginador

        $data['inicio_muestra'] = $cont; //desde donde empieza lo que se va a mostrar  
        $data['cant_muestra'] = 5;  //cantidad por muestra   
        $config['base_url'] = base_url('index.php/notaspedido/index');
        $config['total_rows'] = sizeof($itemsnotas);
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav aria-label="..."> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Siguiente';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);


        $this->load->view('themplate/menu');
        $this->load->view('notaspedido/index', $data);
        $this->load->view('themplate/footerprincipal');
    }

    public function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }

}
