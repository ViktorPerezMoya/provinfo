<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrdenesCompra
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenescompra extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ordencompra_model');
    }

    public function index($cont = 0) {
        $this->checarSesion();
        
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Ordenes de Compra" => "",
        );
        $data['breadcrumb'] = $breadcrumb;

        $data['info'] = "<p>Mediante esta sección del portal puedes acceder a las <strong>Ordenes de Compra</strong> en formato PDF mediante el boton de imprimir situado a la derecha de cada fila.</p><p>Usted puede reacomodar las tablas de la forma que mas le convenga ordenandolas alfabeticamente o de menor a mayor en las columnas donde solo sean numeros o fechas.</p>";
        
        $data['title_head'] = "Mis Ordenes de compra";
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_rules(
                'periodo', 'Numero', 'trim|numeric', array(
            'numeric' => '<p style="color: red;">El %s no es un numero.</p>'
                )
        );
        $this->form_validation->set_rules(
                'detalle', 'Detalle', 'trim', array(
            'alpha_dash' => '<p style="color: red;">El %s no es valido solo se permiten caracters alfanumericos, guion y guion bajo.</p>'
                )
        );

        //traer cuil
        $cuil = $this->session->userdata('cuilprov');
        //traer rubros asignados para usar en filtro
        //traer notas de pedidos segun rubros como array
        if ($this->form_validation->run() == false) {
            $dataquery = $this->session->flashdata('pbusqueda');
            if ($dataquery !== null && $cont > 0) {
                $this->session->set_flashdata('pbusqueda', $dataquery);
                $notas = $this->ordencompra_model->getAll($cuil, $dataquery); //NOTAS CON FILTRO
            } else {
                $notas = $this->ordencompra_model->getAll($cuil, null); //NOTAS SIN FILTRO
            }
        } else {
            $periinfo = $this->input->post('periodo');
            $detanota = $this->input->post('detalle');
            $dataqry = array("periodo" => $periinfo, "detanota" => $detanota);

            $notas = $this->ordencompra_model->getAll($cuil, $dataqry); //NOTAS CON FILTRO
            $data['dataform'] = $dataqry; //para mantener los datos de busqueda en el formulario 
            $this->session->set_flashdata('pbusqueda', $dataqry);
        }

        $this->load->view('themplate/headprincipal', $data);
        $data['ordenes'] = $notas;
        $data['arrayjs'] = array('scripts.js');


        //paginador

        $data['inicio_muestra'] = $cont; //desde donde empieza lo que se va a mostrar  
        $data['cant_muestra'] = 5;  //cantidad por muestra   
        $config['base_url'] = base_url('index.php/ordenescompra/index');
        $config['total_rows'] = sizeof($notas);
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav aria-label="..."> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Siguiente';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);


        $this->load->view('themplate/menu');
        $this->load->view('ordenescompra/index', $data);
        $this->load->view('themplate/footerprincipal');
    }

    public function imprimir($periInfo = null, $codiOrco = null) {

        $this->checarSesion();
        if (empty($periInfo)) {
            $periInfo = date('Y');
        }

        $this->load->model('itemcompra_model');
        $cuil = $this->session->userdata('cuilprov');
        $ordencom = $this->ordencompra_model->find($periInfo, $codiOrco, $cuil);
        if($ordencom == null){
            redirect(base_url('index.php/ordenescompra/'));
        }
        $prove = $this->ordencompra_model->getProveedor($periInfo, $codiOrco, $cuil);
        $items = $this->itemcompra_model->getAll($periInfo, $codiOrco, $cuil);
        $total = 0;
        foreach ($items as $item) {
            $total += $item->ImunItco * $item->CantItco;
        }

        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Municipio de Maipú');
        $pdf->SetTitle('Orden de compra');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
//        $pdf->SetHeaderData('logo_lavalle.png', PDF_HEADER_LOGO_WIDTH, "Municipalidad de Lavalle", 'Orden de compra N° '.$codiOrco, array(0, 0, 0), array(0, 64, 128));
//        $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        ;
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        //Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('courier', '', 10, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage();

        //fijar efecto de sombra en el texto
        //$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        
        // Argentina
//        setlocale(LC_MONETARY, 'es_AR');
        //$fmt = new NumberFormatter('es_AR', NumberFormatter::CURRENCY);
        //preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= "<style type=text/css>";
        $html .= "td{font-size: 9px;} ";
        $html .= ".small{width: 5%;} ";
        $html .= ".large{width: 20%;} ";
        $html .= "table{backgroun-color: #DE8A65;} ";
        $html .= ".provtitle{text-align: center;color: #000; font-weight: bold; background-color: #E7E7E7;font-size: 10px;height: 20px;} ";
        $html .= ".total,.imunitco{text-align: right;} ";
        $html .= "h3{text-align: ceter;}";
        $html .= ".totalhead,.totalfoot{text-align: right;background-color: #4BB4DF;} ";
        $html .= "th{color: #000; font-weight: bold; background-color: #E7E7E7;font-size: 9px;border: 0.1px solid black;} ";//;border: 0.1px solid black;
        $html .= ".impar{background-color: #EBF2F9;}";
        $html .= ".Encabezado{background-color: #DADADA;text-align:center;font-weight:bold;}";
        $html .= "table {border-collapse:collapse}td {border: 0.1px solid black;font-size: 9px;}";
        $html .= "p{font-size: 9px;text-align: justify;margin-top:100%;}";
        $html .= "</style>";
        $html .= "<h3>Orden de Compra N° " . $ordencom->CodiOrco . "</h3>";
        $html .= "<hr>";
        $html .= "<h4>Periodo: <span>" . $ordencom->PeriInfo . "</span></h4>";
        $html .= '<table width="100%">';

        $html .= '<tr>'
                . '<td>Fecha: ' . $ordencom->FechOrco . '</td>'
                . '<td>Tipo de compra: ' . $ordencom->CodiTico . '-' . $ordencom->DetaTico . '</td>'
                . '<td>Expediente: ' . $ordencom->ExpeImpu . '</td>'
                . '<td>Nota de Pedido: ' . $ordencom->CodiNope . '</td>'
                . '<td colspan="2">Area Solicitante: ' . $ordencom->CodiOrga . '-' . $ordencom->DetaOrga . '</td>'
                . '<td>Plazo de Entrega: </td>'
                //. '<td>Usuario: ' . /*$ordencom->AltaUsua .*/ '</td>'
                //. '<td class="totalhead"><strong>Total: ' . $fmt->formatCurrency($total,"  $") . '</strong></td>'
                . '<td class="totalhead"><strong>Total:   <br>$ ' . $this->my_number_format($total,',','.') . '</strong></td>'
                . '</tr>';

        $html .= '<tr><td colspan="8" class="provtitle">Proveedor: ' . $prove->CucuPers . ' - ' . $prove->DetaPers . '</td></tr>';
        $html .= '<tr><td colspan="8">Domicilio: ' . $prove->DecrPers . ' ' . '</td></tr>';
        $html .= '<tr><td colspan="8">Telefono: ' . $prove->TelePers . '</td></tr>';
        $html .= '<tr><td colspan="8">Detalle: ' . $ordencom->DetaOrco . '</td></tr>';
        $html .= '<tr><td colspan="8">Lugar de entrga: ' . $ordencom->DetaUbfi .' - '.$ordencom->DienNope. '</td></tr>';

        //provincias es la respuesta de la función getProvinciasSeleccionadas($provincia) del modelo
        $html .= "<tr>"
                . '<th class="small">Item</th>'
                . '<th class="small">Alt.</th>'
                . '<th class="medium">Cantidad</th>'
                . '<th>Insumo</th>'
                . '<th class="large">Detalle</th>'
                . '<th class="large">Caracteristicas</th>'
                . '<th>Importe Unitario</th>'
                . '<th>Total</th>'
                . '</tr>';
        $impar = false;
        foreach ($items as $item) {
            if ($impar) {
                $html .= '<tr class="item impar">';
                $impar = false;
            } else {
                $html .= '<tr class="item">';
                $impar = true;
            }

            $html .= '<td>' . $item->CodiItco . '</td>';
            $html .= '<td>' . $item->AlteItco . '</td>';
            $html .= '<td>' . $item->CantItco . '</td>';
            $html .= '<td>' . $item->CodiInsu . ' - ' . $item->DetaInsu . '</td>';
            $html .= '<td>' . $item->DetaItco . '</td>';
            $html .= '<td>' . $item->CaraItco . '</td>';
            $html .= '<td class="imunitco"> $ ' . $this->my_number_format($item->ImunItco,',','.')  . '</td>'; 
            //$html .= '<td class="total">' . $fmt->formatCurrency($item->TotalItco, "  $") . '</td
            $html .= '<td class="total"> $ ' . $this->my_number_format($item->TotalItco,',','.') . '</td>';
            //$this->my_number_format($total,',','.')

            $html .= '</tr>';
        }
        $html .= '<tr>'
                //. '<td colspan="8" class="totalfoot"><strong>Total: ' . $fmt->formatCurrency($total,"  $") . '</strong></td>'
                . '<td colspan="8" class="totalfoot"><strong>Total: $  ' . $this->my_number_format($total,',','.') . '</strong></td>'
                . '</td>';
        $html .= "</table>";
        
        if($ordencom->SelloOrco > 0){
            $html .= '<br><br><br><br><br><br><br>'
                    .'<table width="50%" border="1" align="left" cellpadding="1" cellspacing="0" class="tbl_resolucion">'
                    .'  <tr valign="top" class="LineaDato">'
                    .'    <td class="Encabezado">RESOLUCIÓN DGR Nro 35/95</td>'
                    .'  </tr>'
                    .'  <tr>'
                    .'    <td valign="top" class="LineaDato">Condición del Proveedor frente al I.V.A.</td>'
                    .'  </tr>'
                    .'  <tr>'
                    .'    <td valign="top" class="LineaDato">Responsable '.(($prove->FactProv == "fca" || $prove->FactProv == "fcb")? '' : 'no' ).' Inscripto</td>'
                    .'  </tr>'
                    .'  <tr>'
                    .'    <td valign="top" class="LineaDato">Importe Neto sin IVA: $ '.(($prove->FactProv == "fca" || $prove->FactProv == "fcb")? $this->my_number_format($total / (float)(1+((float)$ordencom->AlivaOrco)/100),',','.') : $this->my_number_format($total,',','.') ).'</td>'
                    .'  </tr>'
                    .'  <tr>'
                    .'    <td valign="top" class="LineaDato">Importe IVA: $ '.(($prove->FactProv == "fca" || $prove->FactProv == "fcb")? $this->my_number_format($total - $total / (float)(1+((float)$ordencom->AlivaOrco)/100),',','.') : '0.00' ).'</td>'
                    .'  </tr>';
                $html .= '  <tr>'
                        .'    <td valign="top" class="LineaDato">IMPUESTO A LOS SELLOS: $ '.$this->my_number_format($ordencom->SelloOrco,',','.').'</td>'
                        .'  </tr>';
            $html .= '</table>';
        }
        $html .= '<p>Sr. PROVEEDOR: Ud. deberá entregar y facturar lo detallado en la presente Orden de Compra, la que ha sido confeccionada en base a presupuesto solicitado oportunamente a su firma. En caso de modificaciones NO DEBERÁ hacer entrega de los materiales o prestar el servicio, caso contrario correrá por su cuenta y responsabilidad. Este original deberá presentarse con la factura '
                    . 'correspondiente SIN EXCEPCIÓN.</p><p>Si usted se encuentra beneficiado con tasa cero o tasa reducida según lo establecido por el art.185 del Código Fiscal de la Provincia de Mendoza, deberá presentar en forma mensual el correspondiente certificado.</p>';
        
        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Orden de compra " . $prove->DetaPers . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }

function my_number_format($number, $dec_point, $thousands_sep) 
{ 
  $tmp = explode('.', $number); 
  $out = number_format($tmp[0], 0, $dec_point, $thousands_sep); 
  if ( isset($tmp[1]) ){ 
            if(strlen($tmp[1]) == 1){
                    $out .= $dec_point.substr($tmp[1].'0', 0, 2);
            }else{

                    $out .= $dec_point.substr($tmp[1], 0, 2);
            }
        }else{
            $out .= ',00';
        }

  return $out; 
}

}
