<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('proveedor_model');
    }

    public function index() {
        $data['title_head'] = "Portal del Proveedor - Maipu";
        $this->load->view('home/index', $data);
        //echo phpinfo();
    }

    public function proveedores() {
        $this->checarSesion();
        
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Cambiar Proveedores" => "",
        );
        $data['breadcrumb'] = $breadcrumb;
        
        $data['title_head'] = "Tus Proveedores";
        $this->load->model('usuaprovcuit_model');
        //traer el id del proveedor
        $id = $this->session->userdata('id');
        //trigo los cuils relacionados
        $cuils = $this->usuaprovcuit_model->findAllProvCuit($id);
        $provs = array();
        foreach ($cuils as $c) {
            $prov = $this->proveedor_model->find($c->CucuProv);
            if(!empty($prov)){
                array_push($provs, $prov);
            }
        }
        
        if(sizeof($provs) == 1){
            //var_dump($provs);
            redirect(base_url('index.php/home/bienvenido/'.$provs[0]->CucuPers));
        }
        //var_dump($provs);die();
        $data['provs'] = $provs;
        $this->load->view('themplate/headprincipal', $data);
        $this->load->view('themplate/menusimple');
        $this->load->view('home/proveedores', $data);
        $this->load->view('themplate/footerprincipal');
    }

    public function bienvenido($cuil = null) {
        $this->checarSesion();
        
        $breadcrumb = array(
            "Home" => "", 
        );
        $data['breadcrumb'] = $breadcrumb;
        
        if($cuil == null){
            if($this->session->userdata('cuilprov')){
                $cuil = $this->session->userdata('cuilprov');
            }else{
                redirect(base_url('index.php/home/proveedores'));
            }
        }
        
        $prov = $this->proveedor_model->find($cuil);//valida si el cuil que tengo existe en base de datos y me trae el proveedor, tambien debe comparar si esta asignado al email con el que estoy registrado
        if(empty($prov)){
            redirect(base_url('index.php/auth/login'));
        }
        $userdata = array('cuilprov' => $prov->CucuPers,'nameprov'=> $prov->DetaProv);
        $this->session->set_userdata($userdata);
        $data['title_head'] = "Bienvenido";

        $data['nameprov'] = $this->session->userdata('nameprov');

        $this->load->view('themplate/headprincipal', $data);
        $this->load->view('themplate/menu');
        $this->load->view('home/bienvenido', $data);
        $this->load->view('themplate/footerprincipal');
    }
    
    public function acceso_denegado(){
        $data['title_head']="Acceso Denegado!";
        $this->load->view('themplate/headprincipal', $data);
        $this->load->view('themplate/menu');
        $this->load->view('home/acceso-denegado');
        $this->load->view('themplate/footerprincipal');
    }
            
    function checarSesion(){
        if(!$this->session->userdata('id')){
            redirect(base_url('index.php/auth/login'));
        }
    }

}
