<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cotizacion
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizacion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('notapedido_model');
        $this->load->helper('form');
    }

    public function index() {
        
    }

    public function cotizacion($PeriInfo = 0, $CodiNope = 0) {
        $this->checarSesion();
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Mis Cotizaciones" => "index.php/cotizacion/listar",
            "Cotizacion" => ""
        );
        $data['breadcrumb'] = $breadcrumb;
        $data['info'] ="<p>Aquí usted podrá cotizar cada uno de los items descriptos en la nota de pedido. Si usted tiene un articulo alternativo al solicitado entonces marque la casilla 'Alte' (alternativo) correspondiente al item de la fila.</p><p>Algunas sugerencias: </p><ul><li>Solo se permiten montos unitarios positivos.</li><li>No se permite cantidades mayores a las establecidas.</li><li>Al insertar el monto unitario haga click en el boton <i class='fas fa-check'></i> para calcular el monto total.</li><li>Recuerde <strong>guardar</strong> la cotización antes de  salir de esta sección del portal.</li></ul>";
        $data['arrayjs'] = array('cotizacion.js');
        
        $this->load->model(array('notapedido_model', 'proveedor_model', 'tipocompra_model', 'itemnota_model',
            'itemcotizacion_model', 'cotizacion_model', 'provxrubro_model'));
        $prov = $this->proveedor_model->find($this->session->userdata('cuilprov'));
        $array_misrubs = $this->provxrubro_model->findIdsRubros($prov->CucuPers);
        $notape = $this->notapedido_model->getByCodiNope($PeriInfo, $CodiNope);

        $redirect = true;
        //var_dump($notape);die();
        foreach ($array_misrubs as $r) {
            if ($r['IdRubro'] == $notape->IdRubro) {
                //si encuentro el rubro del la nota de pedido en mis rubros continuo la ejecucion del codigo
                $redirect = false;
                break;
            }
        }
        if ($redirect) {
            $this->session->set_flashdata('mensaje', '<div style="margin-top:30px;text-align:center;" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>Cotizacion no correspondida</p></div>');
            redirect(base_url('index.php/notaspedido/'));
        }
        $tipocom = $this->tipocompra_model->getByCodiTico($notape->CodiTico);
        $itemsnota = $this->itemnota_model->getByCodiNope($prov->CucuPers, $PeriInfo, $CodiNope);
        //guardo cotizacion inicial vacia
//        echo 'NOTAPE';
//        var_dump($notape);echo '<br>PROV';
//        var_dump($prov);echo '<br>TIPOCOM';
//        var_dump($tipocom);echo '<br>ITEMNOTA';
//        var_dump($itemsnota);echo '<br>';
//        die();
        $cotizacion = $this->cotizacion_model->find($prov->CucuPers, $notape);
        //var_dump($cotizacion);die();
        $data['title_head'] = "Cotizacion";
        $this->load->view('themplate/headprincipal', $data);
        $data['notape'] = $notape;
        $data['prov'] = $prov;
        $data['tipocomp'] = $tipocom;
        $data['itemsnota'] = $itemsnota;
        $data['coti'] = $cotizacion;
        $this->load->view('themplate/menu');
        $this->load->view('cotizacion/cotizacion', $data);
        $this->load->view('themplate/footerprincipal');
    }


    public function cotizacion_back() {
        if ($this->session->userdata('id')) {
            if (!empty($this->input->post())) {

                //var_dump($this->input->post());die();

                $cotizacion = $this->input->post('cotizacion');
                $itemscompra = $this->input->post('arrayitc');
                $fechalimite = $this->input->post('fechalimite');
                $this->load->model(array('cotizacion_model', 'itemcotizacion_model','proveedor_model','itemnota_model'));
                $strFecha = $this->cotizacion_model->getHoraServidor();
                //var_dump((trim($fechalimite)) .' compara con '. ($strFecha));
                if(strtotime(trim($fechalimite)) > strtotime($strFecha)){
                    $email = $this->session->userdata('email');
                    //agrego email a la cotizacion es el nombre de usuario
                    $cotizacion['email'] = $email;
                    //agrego mas datos a cada itemcotizacion
                    $arrayitc = array();
                    $arrayitc_upd = array();
                    if(!empty($itemscompra)){
                        foreach ($itemscompra as  $itc){
                            $itc_exist = $this->itemcotizacion_model->existeItemCoti($itc);//duevuelve falso o el objeto
                            if(!$itc_exist){
                                $itemnota = $this->itemnota_model->find($itc);
                                $itc['detaitco'] = $itemnota['DetaItno'];
                                $itc['codiinsu'] = $itemnota['CodiInsu'];
                                array_push($arrayitc, $itc);
                            }else if($itc['caraitco'] != $itc_exist->CaraItco || $itc['imunitco'] != $itc_exist->ImunItco || $itc['cantitco'] != $itc_exist->CantItco || $itc['marcaitco'] != $itc_exist->MarcaItco){
                                array_push($arrayitc_upd, $itc);
                            }
                        }
                        
                        if (!$this->cotizacion_model->existe($cotizacion)) {
                            $exito = $this->cotizacion_model->insertCoti($cotizacion,$arrayitc,$arrayitc_upd);
                        }else{
                            $exito = $this->cotizacion_model->updateCoti($cotizacion,$arrayitc,$arrayitc_upd);
                        }
                        
                        if($exito){
                            echo 'INFORMAR';
                            return;
                        }else{
                            echo 'ERROR';
                            return;
                        }
                    }else{
                        echo 'SIN ITEMS';
                        return;
                    }
                    
                }else{
                    echo 'ERROR EXPIRED';
                    return;
                }
            }
        }else{
            echo 'SESION EXPIRADA';
            return;
        }
        echo 'NO hay datos a guardar';
    }

    public function imprimir($PeriInfo = 0, $CodiNope = 0){
        $this->checarSesion();
        
        $this->load->model(array('notapedido_model', 'proveedor_model', 'tipocompra_model', 'itemnota_model',
            'itemcotizacion_model', 'cotizacion_model', 'provxrubro_model'));
        $prov = $this->proveedor_model->find($this->session->userdata('cuilprov'));
        $notape = $this->notapedido_model->getByCodiNope($PeriInfo, $CodiNope);
        $tipocom = $this->tipocompra_model->getByCodiTico($notape->CodiTico);
        $itemsnota = $this->itemnota_model->getByCodiNope($prov->CucuPers, $PeriInfo, $CodiNope);
        
        
        $this->load->library('Pdf_solicitud');
        $pdf = new Pdf_solicitud('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Municipio de Maipu');
        $pdf->SetTitle('Solicitud de cotizacion');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
//        $pdf->SetHeaderData('logo_lavalle.png', PDF_HEADER_LOGO_WIDTH, "Municipalidad de Lavalle", 'Orden de compra N° '.$codiOrco, array(0, 0, 0), array(0, 64, 128));
//        $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        ;
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        //Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('courier', '', 10, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage();

        //fijar efecto de sombra en el texto
        //$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        
        // Argentina
//        setlocale(LC_MONETARY, 'es_AR');
        //$fmt = new NumberFormatter('es_AR', NumberFormatter::CURRENCY);
        //preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= "<style type=text/css>";
        $html .= "td{font-size: 9px;} ";
        $html .= ".small{width: 5%;} ";
        $html .= ".large{width: 20%;} ";
        $html .= "table{backgroun-color: #DE8A65;} ";
        $html .= ".provtitle{text-align: center;color: #000; font-weight: bold; background-color: #E7E7E7;font-size: 10px;height: 20px;} ";
        $html .= ".total,.imunitco{text-align: right;} ";
        $html .= "h3{text-align: ceter;}";
        $html .= ".totalhead,.totalfoot{text-align: right;background-color: #4BB4DF;} ";
        $html .= "th{color: #000; font-weight: bold; background-color: #E7E7E7;font-size: 9px;border: 0.1px solid black;} ";//;border: 0.1px solid black;
        $html .= ".impar{background-color: #EBF2F9;}";
        $html .= ".Encabezado{background-color: #DADADA;text-align:center;font-weight:bold;}";
        $html .= "table {border-collapse:collapse}td {border: 0.1px solid black;font-size: 9px;}";
        $html .= "h4{font-size: 14px;} ";
        $html .= ".renglon{height: 20px;} ";
        $html .= "p{font-size: 9px;text-align: justify;margin-top:100%;}";
        $html .= "</style>";
        $html .= "<h3>Solicitud de Cotización</h3>";
        $html .= "<p>Domicilio: Pablo Pescara 190 (5515) Maipú - Mendoza
                    <br>Condición del IVA: Exento
                    <br>CUIT: 30-99907873-3
                    <br>Teléfono: 0261 - 4974343
                    <br>Email:</p>";
        $html .= "<hr>";
        $html .= "<br><br>";
        $html .= "<h3><strong>Periodo: </strong>$notape->PeriInfo</h3>";
        $html .= "<table>"
                . "<tr>"
                . "<th><h4>Nota de pedido N° $notape->CodiNope</h4></th>"
                . "<td><p>Fecha de emisión: ".date($notape->FechNope)."</p>"
                . "</td>"
                . "<td>Tipo de compra: <br>$tipocom->DetaTico</td>"
                . "<td>Expediente: $notape->ExpeImpu</td>"
                . '<th>Total:</th>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="5">Detalle: '.$notape->DetaNope.'</td>'
                . "</tr>"
                . "</table>"
                . "<table>"
                . "<tr>"
                . '<th width="30">Item</th>'
                . "<th>Insumo</th>"
                . '<th width="55">Cantidad</th>'
                . '<th width="255">Detalle</th>'
                . "<th>Marca</th>"
                . '<th width="55">Importe unitario</th>'
                . '<th width="55">Total</th>'
                . "</tr>";
        foreach ($itemsnota as $item){
            $html .=  "<tr>"
                . '<td class="renglon">'.$item['CodiItno']."</td>"
                . "<td>".$item['CodiInsu']." - ".$item['DetaInsu']."</td>"
                . "<td>".$this->my_number_format($item['CantItem'])."</td>"
                . "<td>".$item['CaractItem']."</td>"
                . "<td></td>"
                . "<td></td>"
                . "<td></td>"
                . "</tr>";
        }
            $html .=  '<tr>'
                . '<td colspan="7" class="renglon">Monto terminado con IVA incluido</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Forma de pago:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Mantenimiento de la oferta:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Plazo de entrega:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Razón Social:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">CUIT:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Domicilio:</td>'
                . "</tr>"
                . "<tr>"
                . '<td colspan="7" class="renglon">Correo/Mail:</td>'
                . "</tr>"
                . "</table>";
        
        
        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("solicitud_de_cotizacion_".$notape->CodiNope."_".$notape->PeriInfo.".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }
    
    public function registrarcambio() {
        if ($this->session->userdata('id')) {
            $this->checarSesion();
            $fechalimite  = $this->input->post('fechalimite');
            $this->load->model('cotizacion_model');
            $strFecha = $this->cotizacion_model->getHoraServidor();
            if(strtotime(trim($fechalimite)) > strtotime($strFecha)){
                $dataq = array();
                $dataq['periinfo'] = $this->input->post('PeriInfo');
                $dataq['codinope'] = $this->input->post('CodiNope');
                $dataq['cucupers'] = $this->input->post('CucuPers');
                $dataq['codiitno'] = $this->input->post('CodiItno');
                $dataq['alteitco'] = $this->input->post('AlteItco');
                $dataq['cantitco'] = $this->input->post('CantItco');
                $dataq['imunitco'] = $this->input->post('ImunItco');

                $this->load->model(array('itemcotizacion_model'));

                //si solo hay un item y es borrado se borra la cotizacion
                $itemsnota = $this->itemcotizacion_model->getAllItemsCotizado($dataq['cucupers'], $dataq['periinfo'], $dataq['codinope']);
                if(sizeof($itemsnota) == 1){
                    $this->itemcotizacion_model->deleteAll($dataq);
                    $this->cotizacion_model->delete($dataq);
                    $this->session->set_flashdata('mensaje', '<div style="margin-top:30px;text-align:center;" class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>Cotización borrada de forma exitosa!</p></div>');
                
                    
                    echo 'COTIZACION DELETED';
                    return;
                }
                //revisamos si existe cotizacion

                //revisamos si existe
                if ($this->itemcotizacion_model->existeItemCoti($dataq)) {
                    $this->itemcotizacion_model->deleteItemCoti($dataq);
                }
            }else{
                echo 'ERROR EXPIRED';
                return;
            }
        }else{
            echo 'SESION EXPIRADA';
            return;
        }
    }

    public function delcotizacion() {
        if ($this->session->userdata('id')) {
            $fechalimite = $this->input->post('fechalimite');
            $this->load->model('cotizacion_model');
            $strFecha = $this->cotizacion_model->getHoraServidor();
            if(strtotime(trim($fechalimite)) > strtotime($strFecha)){
                if (!empty($_POST['PeriInfo']) && !empty($_POST['CodiNope']) && !empty($_POST['CucuPers'])) {
                    $this->load->model(array('itemcotizacion_model', 'cotizacion_model'));
                    $dataq = array(
                        'periinfo' => $this->input->post('PeriInfo'),
                        'codinope' => $this->input->post('CodiNope'),
                        'cucupers' => $this->input->post('CucuPers')
                    );

                    $this->itemcotizacion_model->deleteAll($dataq);
                    $this->cotizacion_model->delete($dataq);
                    $this->session->set_flashdata('mensaje', '<div style="margin-top:30px;text-align:center;" class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>Cotización borrada de forma exitosa!</p></div>');
                }
            } else {
                echo 'ERROR EXPIRED';
                return;
            }
        }else{
            echo 'SESION EXPIRADA';
            return;
        }
    }

    public function listar($cont = 0) {
        $this->checarSesion();

        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Mis Cotizaciones" => ""
        );
        $data['breadcrumb'] = $breadcrumb;

        $data['info'] = "<p>En esta sección del portal usted podra ver y acceder a todas las <strong>Cotizaciones</strong> que usted ha realizando. Podrá realizar busquedas por año, detalle y tipo de categoría.</p><p> El boton azul colocado a la izquierda de cada fila desplegara la lista de items cotizados y no cotizados, mientras que el boton amarillo situado a la derecha accedera a la cotizacion correspondiente.</p>";

        $data['title_head'] = "Notas de Pedido";
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_rules(
                'periodo', 'Numero', 'trim|numeric', array(
            'numeric' => '<p style="color: red;">El %s no es un numero.</p>'
                )
        );
        $this->form_validation->set_rules(
                'detalle', 'Detalle', 'trim', array(//alpha_dash
            'alpha_dash' => '<p style="color: red;">El %s no es valido solo se permiten caracters alfanumericos, guion y guion bajo.</p>'
                )
        );

        $this->load->model('itemcotizacion_model');
        $this->load->model('provxrubro_model');
        $this->load->model('cotizacion_model');
        //traer cuil
        $cuil = $this->session->userdata('cuilprov');
        //traer rubros asignados para usar en filtro
        $misrubros = $this->provxrubro_model->findRubros($cuil);
        //traer notas de pedidos segun rubros como array
        if ($this->form_validation->run() == false) {
            $dataquery = $this->session->flashdata('pbusqueda');
            if ($dataquery !== null && $cont > 0) {
                $this->session->set_flashdata('pbusqueda', $dataquery);
                $notas = $this->cotizacion_model->getByRubrosXCuil($cuil, $dataquery); //NOTAS CON FILTRO
            } else {
                $notas = $this->cotizacion_model->getByRubrosXCuil($cuil, null); //NOTAS SIN FILTRO
            }
        } else {
            $periinfo = $this->input->post('periodo');
            $detanota = $this->input->post('detalle');
            $detarubro = $this->input->post('selectrubro');
            $dataqry = array("periodo" => $periinfo, "detanota" => $detanota, "detarubro" => $detarubro);

            $notas = $this->cotizacion_model->getByRubrosXCuil($cuil, $dataqry); //NOTAS CON FILTRO
            $data['dataform'] = $dataqry; //para mantener los datos de busqueda en el formulario 
            $this->session->set_flashdata('pbusqueda', $dataqry);
        }
        //traer item de nota de pedido como array y agregarlo a cada nota de pedido
        $itemsnotas = array();//items de las notas de pedido cotizadas
        foreach ($notas as $n) {
            $items = $this->itemcotizacion_model->getFnCotixProvItem($cuil, $n['PeriInfo'], $n['CodiNope']);
            array_push($itemsnotas, $items);
        }
        $this->load->view('themplate/headprincipal', $data);
        $data['misrubros'] = $misrubros;
        $data['notas'] = $notas;
        $data['itemsnota'] = $itemsnotas;
        $data['arrayjs'] = array('scripts.js');


        //paginador

        $data['inicio_muestra'] = (int)$cont; //desde donde empieza lo que se va a mostrar  
        $data['cant_muestra'] = 5;  //cantidad por muestra   
        $config['base_url'] = base_url('index.php/cotizacion/listar');
        $config['total_rows'] = sizeof($notas);
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav aria-label="..."> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Siguiente';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);


        $this->load->view('themplate/menu');
        $this->load->view('cotizacion/listar', $data);
        $this->load->view('themplate/footerprincipal');
    }

    function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }
    function my_number_format($number, $dec_point = ',', $thousands_sep = '.') 
    { 
      $tmp = explode('.', $number); 
      $out = number_format($tmp[0], 0, $dec_point, $thousands_sep); 
      if ( isset($tmp[1]) ){ 
                if(strlen($tmp[1]) == 1){
                        $out .= $dec_point.substr($tmp[1].'0', 0, 2);
                }else{

                        $out .= $dec_point.substr($tmp[1], 0, 2);
                }
            }else{
                $out .= ',00';
            }

      return $out; 
    }

}
