<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorias
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubros extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('provxrubro_model');
    }

    public function index($cont = 0) {
        $this->checarSesion();
        /*desactivo el mostrar rubros*/
        if($this->session->userdata('show_rubros_new')){
            $this->session->set_userdata('show_rubros_new', false);
        }
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Mis Rubros" => "",
        );
        $data['breadcrumb'] = $breadcrumb;

        $data['info'] = "<p>En esta sección del portal usted podrá adherirse o borrarse  de <strong>Rubros</strong>. Para más información descargue el instructivo que aparece acontinuación.</p><p><a href='". base_url('public/pdf/Manual_Mis_Rubros.pdf')."' class='btn btn-danger' target='_blank'><i class='fas fa-download'></i> Descargar Instructivo</a></p>";
        
        $data['title_head'] = "Mis Rubros";
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('rubro_model');
        
        
        $this->form_validation->set_rules(
                'detarubro', 'Rubro', 'trim', array(
            'alpha_dash' => '<p style="color: red;">El %s no es valido solo se permiten caracters alfanumericos, guion y guion bajo.</p>'
                )
        );
        
        $this->load->view('themplate/headprincipal', $data);
        
        //obtengo el cuil almacenado en sesion
        $cuil = $this->session->userdata('cuilprov');
        $rubrosprov = $this->provxrubro_model->findRubros($cuil);
        $data['rubrosprov'] = $rubrosprov;
        //rubros disponibles
        
        if ($this->form_validation->run() == false) {
            $dataquery = $this->session->flashdata('pbusqueda');
            if ($dataquery !== null && $cont > 0) {
                $this->session->set_flashdata('pbusqueda', $dataquery);
                $rubros = $this->rubro_model->findDetaRubro($dataquery);//NOTAS CON FILTRO
            } else {
                $rubros = $this->rubro_model->findAll();//NOTAS SIN FILTRO
            }
        } else {
            $detarubro = $this->input->post('detarubro');
            $dataqry = array("DetaRubro" => $detarubro);

            $rubros = $this->rubro_model->findDetaRubro($dataqry);//NOTAS CON FILTRO
            $data['dataform'] = $dataqry; //para mantener los datos de busqueda en el formulario 
            $this->session->set_flashdata('pbusqueda', $dataqry);
        }
        
        $data['rubros'] = $rubros;
        
        
        $data['inicio_muestra'] = $cont; //desde donde empieza lo que se va a mostrar  
        $data['cant_muestra'] = 5;  //cantidad por muestra 
        $config['base_url'] = base_url('index.php/rubros/index');
        $config['total_rows'] = sizeof($rubros);
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav aria-label="..."> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Siguiente';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        
        $this->load->view('themplate/menu');
        $this->load->view('rubros/index', $data);
        $this->load->view('themplate/footerprincipal');
    }
    
    public function add_rubro($id_rubro = 0){
        $this->checarSesion();
        if($id_rubro >0){
            $cuit = $this->session->userdata('cuilprov');
            $email = $this->session->userdata('email');
            $this->provxrubro_model->add($cuit,$id_rubro,$email);
            redirect(base_url('index.php/rubros/index'));
        }
        return null;
    }
    
    public function remove_rubro($id_rubro = 0){
        $this->checarSesion();
        if($id_rubro >0){
            $cuit = $this->session->userdata('cuilprov');
            $email = $this->session->userdata('email');
            $this->provxrubro_model->remove($cuit,$id_rubro,$email);
            redirect(base_url('index.php/rubros/index'));
        }
        return null;
    }
    
    
    function checarSesion(){
        if(!$this->session->userdata('id')){
            redirect(base_url('index.php/auth/login'));
        }
    }
}
