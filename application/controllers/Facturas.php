<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Facturas
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('factura_model');
    }
    
    public function index($cont = 0){
        $this->checarSesion();
        
        /*desactivo el mostrar notas*/
        if($this->session->userdata('show_facturas_new')){
            $this->session->set_userdata('show_facturas_new', false);
        }
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Mis Facturas" => "",
        );
        $data['breadcrumb'] = $breadcrumb;

        $data['info'] = "<p>Mediante esta sección del portal puedes acceder al estado de sus <strong>Facturas</strong>.</p>";
        
        $data['title_head'] = "Mis Facturas";
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_rules(
                'numefac', 'Numero', 'trim|numeric', array(
            'numeric' => '<p style="color: red;">El %s no es un numero.</p>'
                )
        );
        $this->form_validation->set_rules(
                'periodo', 'Numero', 'trim|numeric', array(
            'numeric' => '<p style="color: red;">El %s no es un numero.</p>'
                )
        );

        //traer cuil
        $cuil = $this->session->userdata('cuilprov');
        //traer rubros asignados para usar en filtro
        //traer notas de pedidos segun rubros como array
        if ($this->form_validation->run() == false) {
            $dataquery = $this->session->flashdata('pbusqueda');
            if ($dataquery !== null && $cont > 0) {
                $this->session->set_flashdata('pbusqueda', $dataquery);
                $facturas = $this->factura_model->getAll($cuil, $dataquery); //NOTAS CON FILTRO
            } else {
                $facturas = $this->factura_model->getAll($cuil, null); //NOTAS SIN FILTRO
            }
        } else {
            $periinfo = $this->input->post('periodo');
            $detanota = $this->input->post('numefac');
            $dataqry = array("periodo" => $periinfo, "numefac" => $detanota);

            $facturas = $this->factura_model->getAll($cuil, $dataqry); //NOTAS CON FILTRO
            $data['dataform'] = $dataqry; //para mantener los datos de busqueda en el formulario 
            $this->session->set_flashdata('pbusqueda', $dataqry);
        }

        $this->load->view('themplate/headprincipal', $data);
        $data['ordenes'] = $facturas;
        $data['arrayjs'] = array('scripts.js');


        //paginador

        $data['inicio_muestra'] = $cont; //desde donde empieza lo que se va a mostrar  
        $data['cant_muestra'] = 5;  //cantidad por muestra   
        $config['base_url'] = base_url('index.php/facturas/index');
        $config['total_rows'] = sizeof($facturas);
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav aria-label="..."> <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Ultimo';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Siguiente';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);


        $this->load->view('themplate/menu');
        $this->load->view('facturas/index', $data);
        $this->load->view('themplate/footerprincipal');
    }

    function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }

}
