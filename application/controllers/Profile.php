<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Perfil
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('usuaprov_model');
    }

    public function settings() {
        $this->checarSesion();
        
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "Settings" => "",
        );
        $data['breadcrumb'] = $breadcrumb;
        
        $this->load->library('form_validation');

        $this->form_validation->set_rules(
                'passuser', 'Clave', 'required|trim', array(
            'required' => '<p style="color: red;">La %s no puede ser vacio.</p>'
                )
        );

        $this->form_validation->set_rules(
                'passuserrepeat', 'Clave', 'required|trim|matches[passuser]', array(
            'required' => '<p style="color: red;">La %s no puede ser vacio.</p>',
            'matches' => '<p style="color: red;">Las claves no coinciden</p>'
                )
        );
        $data['title_head'] = "Settings";

        $email = $this->session->userdata('email');
        //var_dump($email);die();
        $usuario = $this->usuaprov_model->findUserByEmail($email);


        if ($this->form_validation->run() == false) {

            $data['usuario'] = $usuario;

            $this->load->view('themplate/headprincipal', $data);
            $this->load->view('themplate/menu');
            $this->load->view('profile/settings', $data);
            $this->load->view('themplate/footerprincipal');
        } else {
            $password = $this->input->post('passuser');
            $passwordr = $this->input->post('passuserrepeat');
            if ($password == $passwordr) {
                $usuario->ClavUspro = md5($password);
                $this->usuaprov_model->updateUser($usuario);
                //cargmos la vista
                //
                $this->session->set_flashdata('success_settings', 'La clave fue modificada con éxito.');

                redirect(base_url() . 'index.php/profile/settings', 'refresh');
            } else {
                $this->session->set_flashdata('error_settings', 'Las contrasñas no coiciden');
                redirect(base_url() . 'index.php/profile/settings', 'refresh');
            }
        }
    }

    function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }

}
