<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author VICTOR
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('usuaprov_model');
    }

    public function login() {
        $this->checarSesionLogin();
        $data['title_head'] = "Login";
        $this->load->library('form_validation');


        $this->form_validation->set_rules(
                'emailuser', 'Email', 'required|trim', array(
            'required' => '<p style="color: red;">El %s no puede ser vacio.</p>'
                )
        );

        $this->form_validation->set_rules(
                'passuser', 'Clave', 'required|trim', array(
            'required' => '<p style="color: red;">La %s no puede ser vacio.</p>'
                )
        );

        if ($this->form_validation->run() == false) {
        $this->load->view('themplate/head',$data);
            $this->load->view('auth/login', $data);
        $this->load->view('themplate/footer');
        } else {
            $username = $this->input->post('emailuser');
            $password = $this->input->post('passuser');
            $user = $this->usuaprov_model->findUser($username, $password);
            if ($user == "El Usuario NO EXISTE. Verifique..." ) {
                $this->session->set_flashdata($user);
                redirect(base_url() . 'index.php/auth/login', 'refresh');
            }else if($user->UsuaUspro != "" || $user == "El Usuario tiene la clave básica 123456..."){
                $datauser = array(
                    'id' => $user->IdUspro,
                    'email' => $user->UsuaUspro,
                    'ultimasesion' => $user->UlseUspro,
                    'show_facturas_new' => true,
                    'show_rubros_new' => true,
                    'show_notape_new' =>true);
                $this->session->set_userdata($datauser);
                $user->UlseUspro = $this->usuaprov_model->getHoraServidor();
                $this->usuaprov_model->updateUltiSesi($user);
                redirect(base_url() . 'index.php/home/proveedores');
            } else {
                $this->session->set_flashdata('error_login', 'Usuario o contraseña incorrectos');
                redirect(base_url() . 'index.php/auth/login', 'refresh');
            }
        }
    }

    public function recuperar_pass() {
        $data['title_head'] = "Recupera contraseña";
        $this->load->library('form_validation');


        $this->form_validation->set_rules(
                'emailuser', 'Email', 'required|trim|valid_email', array(
            'required' => '<p style="color: red;">El %s no puede ser vacio.</p>',
            'valid_email' => "El %s no es un email valido"
                )
        );


        if ($this->form_validation->run() == false) {
            $this->load->view('themplate/head',$data);
            $this->load->view('auth/recuperar_pass', $data);
            $this->load->view('themplate/footer');
        } else {
            $username = $this->input->post('emailuser');
            $user = $this->usuaprov_model->findUserByEmail($username);
            if ($user) {
                //generar nuevo passs
                $newpass = $this->generateRandomString(8);
                //guardar nueva pass
                $user->ClavUspro = md5($newpass);
                $updated = $this->usuaprov_model->updateUser($user);
                if(!$updated){
                    show_error("Ha fallado la actualizacion de la nueva clave.");
                }
                //enviar por emial nueva pass
                $sended = $this->enviar_email($user->UsuaUspro, $newpass);
                if(!$sended){
                    show_error("El envio del e-mail ha fallado ");
                }

                //cargmos la vista
                $this->session->set_flashdata('success_recovery', 'La clave fue reestablecida. Se ha enviado un email a <strong>' . $user->UsuaUspro . '</strong> con su nueva clave');
                redirect(base_url() . 'index.php/auth/recuperar_pass', 'refresh');
            } else {
                $this->session->set_flashdata('error_recovery', 'No existe el usuario');
                redirect(base_url() . 'index.php/auth/recuperar_pass', 'refresh');
            }
        }
    }

    public function logout() {
        var_dump($this->session->userdata('cuit'));
        if ($this->session->userdata('email') == '') {
            redirect(base_url('index.php/auth/login'));
        }
        $this->session->sess_destroy();
        redirect(base_url() . '');
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function enviar_email($email, $clave) {
            $this->load->library('email');

            $configuration = array(
                'protocol' => 'smtp',
                'smtp_host' => 'infogov.com.ar',
                'smtp_port' => 25,
                'smtp_user' => 'maipu',
                'smtp_pass' => 'noessolomaipu',
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n"
            );

            $this->email->initialize($configuration);

            $this->email->from('maipu@infogov.com.ar', 'Municipalidad de Maipú');
            $this->email->to($email);

            $this->email->subject('Cambio de clave');
            $this->email->message('<div style="background-color: #CECEF6; padding: 30px;border-radius: 4px;">'
                    . '<h1 style="color: black;">Cambio e clave</h1>'
                    . '<p style="color: black;">Se ha procesado su socilitud de cambio de clave.</p>'
                    . '<p>Su nueva clave es: <b>' . $clave . '</b></p>'
                    . '<div style="padding: 20px; border-radius: 4px; background-color: #F6CECE"><h3 style="color: #FA5858;">Importante!</h3><p  style="color: black;">Se recomienda cambiar la clave una vez iniciada la sesion.</p></div>'
                    . '<center><img src="https://infogov.com.ar/proveedores/maipu/public/img/logo_maipu.png" style="margin: 20px"/></center>'
                    . '</div>');

            return $this->email->send();
    }

    function checarSesionLogin() {
        if ($this->session->userdata('id')) {
            redirect(base_url('index.php/home/proveedores'));
        }
    }

    public function checarSesion() {
        if (!$this->session->userdata('id')) {
            redirect(base_url('index.php/auth/login'));
        }
    }

}
