<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mail
 *
 * @author VICTOR
 */
class Mail extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('usuaprov_model');  
    }
    public function index(){
        $this->checarSesion();
        
        $breadcrumb = array(
            "Home" => "index.php/home/bienvenido",
            "E-mails Masivos" => "",
        );
        $data['breadcrumb'] = $breadcrumb;
        
        $data['title_head'] = "Envio de e-mails";
        
        $this->load->view('themplate/headprincipal', $data);
        $this->load->view('themplate/menu');
        $this->load->view('mail/index', $data);
        $this->load->view('themplate/footerprincipal');
    }
    
    public function send_email(){
        $this->checarSesion();
        $data['title_head'] = "Enviando...";
        $this->load->view('themplate/headprincipal', $data);
        $this->load->view('mail/sending');
        $this->load->view('themplate/footerprincipal');
        // Creamos la sentencias SQL 
        $this->load->library('email');

        $configuration = array(
            'protocol' => 'smtp',
            'smtp_host' => 'infogov.com.ar',
            'smtp_port' => 25,
            'smtp_user' => 'maipu',
            'smtp_pass' => 'noessolomaipu',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        $this->email->initialize($configuration);

        $users = $this->usuaprov_model->getAll();
        //var_dump($users);die();
        
        $bool = false;
        foreach ($users as $row){
            try{
            $this->email->from('maipu@infogov.com.ar', 'Municipalidad de Maipú');
            $this->email->to($row['su_usuario']);

            $this->email->subject('Portal de Compras Web - Municipalidad de Maipú');
            $this->email->message('<div style="background-color: #f3f3f3;padding-top: 50px;padding-bottom: 50px;padding-left: 100px;padding-right: 100px;border-radius: 3px;box-shadow: 2px 2px 5px #999;color:black;">'
                    . '<h1 style="color: black;">Bienvenido al Portal de Compras Web de la Municipalidad de Maipú.</h1>'
                    . '<p style="color: black;">Sr. Proveedor: '.$row['sr_sroveedor'].'</p>'
                    . '<p style="color: black;text-align: justify;">El presente portal le permite realizar sus cotizaciones, relacionadas con las compras directas, de una manera más ágil  y principalmente mostrar la transparencia en nuestros procesos.</p>'
                    . '<p style="color: black;text-align: justify;">A través de esta plataforma Ud. podrá realizar ofertas, además de consultar sus cotizaciones, ordenes de compras y el estado de sus facturas. Permitiéndole contar con información personalizada y actualizada.</p>'
                    . '<p style="color: black;text-align: justify;">Con su usuario y contraseña podrá ingresar al "PORTAL DEL PROVEEDOR - Compras Online" (<a href="http://maipu.gob.ar/compras-y-licitaciones/">http://maipu.gob.ar/compras-y-licitaciones/</a>) donde encontrará el instructivo con los pasos a seguir.</p>'
                    . '<p style="color: black;">Su nombre de usuario es: <b>' . $row['su_usuario'] . '</b></p>'
                    . '<p style="color: black;">Su clave es: <b>' . $row['primer_clave'] . '</b></p>'
                    . '<p style="color: black;font-size:16px;">Recuerde agregarse en todos los rubros de su interés. En breve le enviaremos un correo avisandole las novedades.</p><br><br><br>'
                    . '<center><img src="https://infogov.com.ar/proveedores/maipu/public/img/logo_maipu.png" style="margin: 20px"/></center>'
                    . '</div>');

            $bool = $this->email->send();
            $this->usuaprov_model->notificado($row['su_usuario']);
            }catch(Exception $e){
                var_dump($e->getMessage());die();
            }
        }
         if($bool){
           
            $this->session->set_flashdata('success_settings', 'Los mails han sido enviados a '.sizeof($users).' destinatarios');

            redirect(base_url() . 'index.php/mail/index', 'refresh');
        } else {
            if(sizeof($users) == 0){
                $this->session->set_flashdata('error_settings', 'No hay mails a notificar');
            }else{
                $this->session->set_flashdata('error_settings', 'Ocurrio un error, los mails no se lograron enviar a sus destinatarios');
            }
            redirect(base_url() . 'index.php/mail/index', 'refresh');
        }
        
    }

    
    function checarSesion(){
        if(!$this->session->userdata('id')){
            redirect(base_url('index.php/auth/login'));
        }
        $email = $this->session->userdata('email');
        if($email != "victor.ariel.perez@gmail.com" && $email != "mauromoreno@infogov.com.ar"){
            redirect(base_url('index.php/home/acceso_denegado'));
        }
    }
}


        /*
        
            $this->email->from('maipu@infogov.com.ar', 'Municipalidad de Maipú');
            $this->email->to($row['UsuaUspro']);

            $this->email->subject('Notificación importante');
            $this->email->message('<div style="background-color: #CECEF6; padding: 30px;border-radius: 4px;">'
                    . '<h1 style="color: black;">Información de cuenta creada</h1>'
                    . '<p style="color: black;">Sr. proveedor se le informa que se encuentra activasda su cuenta en el <a href="http://infogov.com.ar/proveedores/maipu/auth/login">Portal del Proveedor</a>.</p>'
                    . '<p>Su nombre de usuario es: <b>' . $row['UsuaUspro'] . '</b></p>'
                    . '<p>Su clave es: <b>' . $row['ClavTxtUspro'] . '</b></p>'
                    . '<div style="padding: 20px; border-radius: 4px; background-color: #F6CECE"><h3 style="color: #FA5858;">Importante!</h3><p  style="color: black;">Se recomienda cambiar la clave una vez iniciada la sesion.</p></div>'
                    . '<center><img src="https://infogov.com.ar/proveedores/maipu/public/img/logo_maipu.png" style="margin: 20px"/></center>'
                    . '</div>');

            $this->email->send();

            // Borramos el destinatario, de esta forma nuestros clientes no ven los correos de las otras personas y parece que fuera un único correo para ellos. 
             

         *          */

/*
        $curl = curl_init('https://infogov.ip-zone.com/ccm/admin/api/version/2/&type=json');
        $json = false;
        
        foreach ($users as $item){
            $rcpt = array();
            array_push($rcpt, 
                    array(
                        'name' => $item['UsuaUspro'],
                        'email' => $item['UsuaUspro']
                    )
            );
            
            
            $postData = array(
                'function' => 'sendMail',
                'apiKey' => '1RSnM0qmEoubxQqxORyQqpI24tfa2AWtRpXefDGk',
                'subject' => 'Notificación importante',
                'html' => '<div style="background-color: #CECEF6; padding: 30px;border-radius: 4px;">'
                    . '<h1 style="color: black;">Información de cuenta creada</h1>'
                    . '<p style="color: black;">Sr. proveedor se le informa que se encuentra activasda su cuenta en el <a href="http://infogov.com.ar/proveedores/maipu/">Portal del Proveedor</a>.</p>'
                    . '<p>Su nombre de usuario es: <b>' . $item['UsuaUspro'] . '</b></p>'
                    . '<p>Su clave es: <b>' . $item['ClavTxtUspro'] . '</b></p>'
                    . '<center><img src="https://infogov.com.ar/proveedores/maipu/public/img/logo_maipu.png" style="margin: 20px"/></center>'
                    . '</div>',
                'mailboxFromId' => 1,
                'mailboxReplyId' => 1,
                'mailboxReportId' => 1,
                'packageId' => 6,
                'emails' => $rcpt
            );
            $post = http_build_query($postData);

            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            $json = curl_exec($curl);
            if ($json === false) {
                die('Request failed with error: '. curl_error($curl));
            }

            $result = json_decode($json);
            if ($result->status == 0) {
                die('Bad status returned. Error: '. $result->error);
            }
        }

        if($result->data){
           
            $this->session->set_flashdata('success_settings', 'Los mails han sido enviados');

            redirect(base_url() . 'index.php/mail/index', 'refresh');
        } else {
            $this->session->set_flashdata('error_settings', 'Ocurrio un error, los mails no se lograron enviar a sus destinatarios');
            redirect(base_url() . 'index.php/mail/index', 'refresh');
        }
 * 
  */
 