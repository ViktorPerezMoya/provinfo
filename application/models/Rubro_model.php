<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rubro
 *
 * @author VICTOR
 */
class Rubro_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function findAll(){
            
            $query = $this->db->query("SELECT b.IdRubro,b.DetaRubro, b.AltaFeho FROM rubro b WHERE b.BajaFeho IS NULL ORDER BY b.AltaFeho DESC");
            $result = $query->result();
            $query->free_result();
            return $result;
    }
    
    public function findDetaRubro($dataquery){
            $parametro = $dataquery['DetaRubro'];
            $query = $this->db->query("SELECT b.IdRubro,b.DetaRubro, b.AltaFeho FROM rubro b WHERE b.DetaRubro like '%$parametro%' AND b.BajaFeho IS NULL ORDER BY b.AltaFeho DESC");
            $result = $query->result();
            $query->free_result();
            return $result;
    }
    
}
