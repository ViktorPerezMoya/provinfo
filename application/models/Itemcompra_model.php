<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Itemcompra_model
 *
 * @author VICTOR
 */
class Itemcompra_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAll($periInfo = null, $codiOrco = null, $cucuPers = null) {
        if (isset($periInfo) && isset($codiOrco) && isset($cucuPers)) {
            $query = $this->db->query("SELECT ic.CodiItco, ic.AlteItco, ROUND(ic.CantItco,0) AS CantItco, ic.CodiInsu, ins.DetaInsu, ic.DetaItco,ic.CaraItco,ic.ImunItco,ROUND(ic.CantItco*ic.ImunItco,3) AS TotalItco"
                    . " FROM itemcompra ic"
                    . " INNER JOIN ordencompra oc ON oc.PeriInfo = ic.PeriInfo AND oc.CodiOrco = ic.CodiOrco"
                    . " INNER JOIN infogov.insumo ins ON ins.CodiInsu = ic.CodiInsu"
                    . " WHERE oc.PeriInfo = $periInfo AND oc.CodiOrco = $codiOrco AND oc.Cucupers = $cucuPers;");
            $result =  $query->result();
            $query->free_result();
            return $result;
        }
        return null;
    }

}
