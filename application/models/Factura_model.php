<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Factura_model
 *
 * @author VICTOR
 */
class Factura_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAll($cucuPers = null, $dataquery = null) {

        if (empty($dataquery['periodo'])) {
            $dataquery['periodo'] = date('Y');
        }

        $this->reconectar();
        $query = $this->db->query("CALL fnFactxProv(?,?)", array($cucuPers, $dataquery['periodo']));
        $array = $query->result_array();
        $query->free_result();

        if(!empty($dataquery['numefac'])){
            $arr_res = array();
            foreach ($array as $item){
                if($item['nro_factura'] == $dataquery['numefac']){
                    array_push($arr_res, $item);
                }
            }
            
            return $arr_res;
        }

        return $array;
    }
    
    private function reconectar() {
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
    }

}
