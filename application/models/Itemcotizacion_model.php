<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Itemcotizacion
 *
 * @author VICTOR
 * call fnCotixProv('33707427189',2018,2315);
 */
class Itemcotizacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function getFnCotixProvItem($cuit = 0, $PeriInfo = 0, $CodiNope = 0) {
        if ($CodiNope > 0) {
//            }
            $this->reconectar();
            $query = $this->db->query("CALL fnCotixProvItem(?,?,?)", array($cuit, $PeriInfo, $CodiNope));
            $array =  $query->result_array();
            $query->free_result();
            return $array;
        }
        return null;
    }
    
    public function getAllItemsCotizado($cuit = 0, $periInfo = 0, $codiNope = 0) {
        //var_dump($cuit);die();
        if ($cuit > 0 && (int) $periInfo > 0 && $codiNope > 0) {
            $this->db->select('*');
            $this->db->where('PeriInfo',$periInfo);
            $this->db->where('CodiNope',$codiNope);
            $this->db->where('CucuPers',$cuit);
            $query = $this->db->get('itemcotizacion');
            
//            $query = $this->db->query("select * from itemcotizacion where PeriInfo = " . $periInfo . " "
//                    . "and CodiNope = " . $codiNope . " "
//                    . "and CucuPers = " . $cuit . ";");
            $array = $query->result_array();
            $query->free_result();
            return $array;
        }
        return null;
    }

    public function existeItemCoti($data = NULL) {
        if($data != null){
            
            $this->db->select('*');
            $this->db->where('PeriInfo',$data['periinfo']);
            $this->db->where('CodiNope',$data['codinope']);
            $this->db->where('CucuPers',$data['cucupers']);
            $this->db->where('CodiItco',$data['codiitno']);
            $this->db->where('AlteItco',$data['alteitco']);
            $query = $this->db->get('itemcotizacion');
            
//            $query = $this->db->query("select * from itemcotizacion where"
//                    . " PeriInfo = ".$data['periinfo']
//                    ." and CodiNope = ".$data['codinope']
//                    ." and CucuPers = ".$data['cucupers']
//                    ." and CodiItco = ".$data['codiitno']
//                    ." and AlteItco = ".$data['alteitco'].";");
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $query->free_result();
                return $row;
            } else {
                //var_dump($this->db->last_query() );die();
                $query->free_result();
                return false;
            }
            
        }
    }

    public function deleteItemCoti($array = null) {
        if ($array != null) {
            $this->db->where('PeriInfo', $array['periinfo']);
            $this->db->where('CodiNope', $array['codinope']);
            $this->db->where('CucuPers', $array['cucupers']);
            $this->db->where('CodiItco', $array['codiitno']);
            $this->db->where('AlteItco', $array['alteitco']);
            $this->db->delete('itemcotizacion');
        }
        return false;
    }
    
    
    public function deleteAll($array = null) {
        if ($array != null) {
            $this->db->where('PeriInfo', $array['periinfo']);
            $this->db->where('CodiNope', $array['codinope']);
            $this->db->where('CucuPers', $array['cucupers']);
            $this->db->delete('itemcotizacion');
        }
        return false;
    }
    
    private function reconectar() {
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
    }

}
