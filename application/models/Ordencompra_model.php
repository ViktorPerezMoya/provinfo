<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ordencompra_model
 *
 * @author VICTOR
 */
class Ordencompra_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAll($cucuPers = null, $dataquery = null) {

        $sqlconcat = "";
        if ($dataquery != null) {
            if ($dataquery['periodo'] != '') {
                $sqlconcat.= " and oc.PeriInfo = " . $dataquery['periodo'];
            }
            if ($dataquery['detanota'] != '') {
                $sqlconcat.= " and oc.DetaOrco like '%" . $dataquery['detanota'] . "%'";
            }
        } else {
            $sqlconcat.= " and oc.PeriInfo = " . date('Y');
        }

//        echo "SELECT oc.PeriInfo,oc.CodiOrco, oc.DetaOrco,tc.DetaTico, o.DetaOrga,oc.ExpeImpu, oc.FechOrco"
//                    . " FROM ordencompra oc"
//                    . " INNER JOIN tipocompra tc ON tc.Coditico = oc.CodiTico"
//                    . " INNER JOIN infogov.organigrama o ON o.CodiOrga = oc.CodiOrga AND o.PeriInfo = oc.PeriInfo"
//                    . " WHERE oc.CucuPers = $cucuPers$sqlconcat  ORDER BY oc.CodiOrco DESC;";die();

        if (!empty($cucuPers)) {
            $query = $this->db->query("SELECT oc.PeriInfo,oc.CodiOrco, oc.DetaOrco,tc.DetaTico, o.DetaOrga,oc.ExpeImpu, oc.FechOrco, oc.FeanOrco"
                    . " FROM ordencompra oc"
                    . " INNER JOIN itemcompra itc ON itc.PeriInfo = oc.PeriInfo AND itc.CodiOrco = oc.CodiOrco AND itc.IdImpu != 0"
                    . " INNER JOIN tipocompra tc ON tc.Coditico = oc.CodiTico"
                    . " INNER JOIN infogov.organigrama o ON o.CodiOrga = oc.CodiOrga AND o.PeriInfo = oc.PeriInfo"
                    . " WHERE oc.CucuPers = $cucuPers$sqlconcat"
                    . " GROUP BY oc.CodiOrco"
                    . " ORDER BY oc.FechOrco DESC, oc.CodiOrco DESC;");
            $array =  $query->result_array();
            $query->free_result();
            return $array;
        }
    }

    public function find($periInfo = null, $codiOrco = null, $cucuPers = null) {
        if (isset($periInfo) && isset($codiOrco) && isset($cucuPers)) {
            $query = $this->db->query("SELECT oc.PeriInfo, oc.CodiOrco,oc.DetaOrco, DATE_FORMAT(oc.FechOrco,'%d-%m-%Y') AS FechOrco, oc.CodiTico, tc.DetaTico, oc.ExpeImpu, oc.CodiNope, oc.CodiOrga, og.DetaOrga, oc.AltaUsua, oc.AlivaOrco, oc.SelloOrco, np.DienNope,IF(np.IdUbfi > 0,(SELECT uf.DetaUbfi FROM ubicfisica uf WHERE uf.IdUbfi = np.IdUbfi),'') AS DetaUbfi"
                    . " FROM ordencompra oc"
                    . " INNER JOIN tipocompra tc ON tc.CodiTico = oc.CodiTico "
                    . " INNER JOIN notapedido np ON np.CodiNope = oc.CodiNope AND np.PeriInfo = oc.PeriInfo"
                    . " INNER JOIN infogov.organigrama og ON og.CodiOrga = oc.CodiOrga AND og.PeriInfo = oc.PeriInfo"
                    . " WHERE oc.PeriInfo = $periInfo AND oc.CodiOrco = $codiOrco AND oc.Cucupers = $cucuPers;");
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

    public function getProveedor($periInfo = null, $codiOrco = null, $cucuPers = null) {
        if (isset($periInfo) && isset($codiOrco) && isset($cucuPers)) {
            $query = $this->db->query("SELECT pr.CucuPers, pr.DetaProv as DetaPers, p.DecrPers, p.TelePers, pr.FactProv  FROM infogov.persona p"
                    . " INNER JOIN proveedor pr ON pr.CucuPers = p.CucuPers"
                    . " INNER JOIN ordencompra oc ON oc.CucuPers = p.CucuPers"
                    . " WHERE oc.PeriInfo = $periInfo AND oc.CodiOrco = $codiOrco AND p.Cucupers = $cucuPers;");
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

}
