<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Provxrubro
 *
 * @author VICTOR
 */
class Provxrubro_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function findRubros($cuil = null){
        if($cuil != null){
            $query = $this->db->query("SELECT b.IdRubro,b.DetaRubro FROM rubro b"
                    . " WHERE b.IdRubro IN ("
                    . " SELECT a.IdRubro FROM provxrubro a WHERE a.CucuPers = ".$cuil." AND a.BajaFeho IS NULL "
                    . ") ORDER BY b.DetaRubro ASC;");
            
            /*if (mysqli_more_results($this->db->conn_id)) {
                mysqli_next_result($this->db->conn_id);
            }
            $query = $this->db->query("CALL fnRubrosxCuit(?)",array($cuil));*/
            $return = $query->result();
            $query->free_result();
            return $return;
        }
        return NULL;
    }
    
    public function findIdsRubros($cuil = null){
        if($cuil != null){
            $query = $this->db->query("SELECT b.IdRubro FROM rubro b"
                    . " WHERE b.IdRubro IN ("
                    . " SELECT a.IdRubro FROM provxrubro a WHERE a.CucuPers = ".$cuil." AND a.BajaFeho IS NULL "
                    . ") ORDER BY b.DetaRubro ASC;");
            $array = $query->result_array();
            $query->free_result();
            return $array;
        }
        return NULL;
    }
    
    public function add($cuit,$idrubro,$user){
        //date("Y-m-d H:i:s");
        $data = array(
            'CucuPers' => $cuit,
            'IdRubro' => $idrubro,
            'AltaUsua' => $user
        );

        return $this->db->insert('provxrubro', $data);
    }
    
    public function remove($cuit,$idrubro,$user){
        //date("Y-m-d H:i:s");
//        $data = array(
//            'BajaUsua' => $user,
//            'BajaFeho' => date("Y-m-d H:i:s"),
//        );
//
//        $this->db->where('CucuPers', $cuit);
//        $this->db->where('IdRubro', $idrubro);
//
//        return $this->db->update('provxrubro', $data);
        
        $this->db->where('CucuPers', $cuit);
        $this->db->where('IdRubro', $idrubro);

        return $this->db->delete('provxrubro');
    }
}
