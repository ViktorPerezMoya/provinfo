<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Organigrama_model
 *
 * @author VICTOR
 */
class Organigrama_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function getAll(){
        $query = $this->db->query("SELECT PeriInfo, CodiOrga, DetaOrga FROM infogov.organigrama ORDER BY DetaOrga ASC;");
        $resilt = $query->result();
        $query->free_result();
        return $resilt;
    }
}
