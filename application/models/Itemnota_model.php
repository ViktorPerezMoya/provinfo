<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * SELECT
  `PeriInfo`,
  `CodiNope`,
  `CodiItno`,
  `CodiInsu`,
  `DetaItno`,
  `CaraItno`,
  `CantItno`,
  `ImunItno`,
  `CodiPart`,
  `CodiOrga`,
  `CodiUnga`,
  `CodiFifu`,
  `IdImpu`
  FROM `contpres_prueba`.`itemnota`
  LIMIT 0, 1000;
 */

/**
 * Description of Itemnota
 *
 * @author VICTOR
 */
class Itemnota_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getByCodiNope($cuit = 0, $PeriInfo = 0, $CodiNope = 0) {
        if ($CodiNope > 0) {
            /*
            $query = $this->db->query("SELECT itn.PeriInfo, itn.CodiNope, itn.CodiItno, itn.CodiInsu, DetaInsu, IFNULL(itc.AlteItco,0) AlteItem"
            .", IFNULL(itc.DetaItco,itn.DetaItno) DetaItem, IFNULL(itc.CaraItco,itn.CaraItno) CaractItem"
            .", IFNULL(itc.CantItco,itn.CantItno) CantItem, IFNULL(itc.ImunItco,0) ImpoUnitItem"
            ." FROM itemnota itn"
            ." INNER JOIN infogov.insumo i ON i.CodiInsu = itn.CodiInsu"
            ." LEFT JOIN itemcotizacion itc ON itc.PeriInfo = itn.PeriInfo AND itc.CodiNope = itn.CodiNope AND itc.CodiItco = itn.CodiItno AND itc.CucuPers = $cuit"
            ." WHERE itn.PeriInfo = $PeriInfo AND itn.CodiNope = $CodiNope ORDER BY itn.CodiNope DESC");
            */
//            if (mysqli_more_results($this->db->conn_id)) {
//                mysqli_next_result($this->db->conn_id);
//            }
            $this->reconectar();
            $query = $this->db->query("CALL fnNotaxProvItem(?,?,?)", array($cuit, $PeriInfo, $CodiNope));
            $array =  $query->result_array();
            $query->free_result();
            return $array;
        }
        return null;
    }

    public function find($data = NULL) {
        if ($data != null) {
            $query = $this->db->query("SELECT  `PeriInfo`,  `CodiNope`,  `CodiItno`,  `CodiInsu`,  `DetaItno`,  `CaraItno`,  `CantItno`, `ImunItno`,  `CodiPart`,  `CodiOrga`,  `CodiUnga`,  `CodiFifu`,  `IdImpu` "
                    . " FROM `itemnota` WHERE `PeriInfo` = " . $data['periinfo'] . " AND `CodiNope` = " . $data['codinope'] . " AND `CodiItno` = " . $data['codiitno']);
            return $query->row_array();
        }
        return null;
    }
    
    private function reconectar() {
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
    }

}
