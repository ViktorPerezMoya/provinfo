<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Proveedor
 *
 * @author VICTOR
 */
class Proveedor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function find($cuil = null) {
        if ($cuil != null) {
            $query = $this->db->query("SELECT p.CucuPers, p.DetaProv FROM proveedor p"
                    . " INNER JOIN usuaprovcuit uc ON uc.CucuProv  = p.CucuPers"
                    . " INNER JOIN usuaprov up ON up.IdUspro = uc.IdUspro"
                    . " WHERE p.CucuPers = $cuil LIMIT 1;");
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

}
