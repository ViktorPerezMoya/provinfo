<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuaprovcuit
 *
 * @author VICTOR
 */
class Usuaprovcuit_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    
    public function findAllProvCuit($idUspro = null){
        if ($idUspro != null) {
            $query = $this->db->query("SELECT IdUspro,CucuProv FROM usuaprovcuit"
                    . " WHERE IdUspro = " . $idUspro." LIMIT 0,1000");
            $result = $query->result();
            $query->free_result();
            return $result;
        }
        return null;
    }
    
}
