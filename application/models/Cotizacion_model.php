<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cotizacion
 *
 * @author VICTOR
 */
class Cotizacion_model extends CI_Model { 

    public function __construct() {
        parent::__construct();
    }

    public function insertCoti($cotizacion = array(), $arrayitc = null, $arrayitc_upd = null) {

        $this->db->trans_begin();
        $data = array(
            'PeriInfo' => $cotizacion['periinfo'],
            'CodiNope' => $cotizacion['codinope'],
            'CucuPers' => $cotizacion['cucupers'],
            'FechCoti' => date('Y-m-d'),
            'PlenCoti' => $cotizacion['plencoti'],
            'ObseCoti' => $cotizacion['obsecoti'],
            'AltaUsua' => $cotizacion['email'],
            'FemaofCoti' => $cotizacion['femaofcoti']
        );
        $this->db->insert('cotizacion', $data);
        //inserta nuevos items
        foreach ($arrayitc as $itc) {
            $dataitc = array(
                'PeriInfo' => $itc['periinfo'],
                'CodiNope' => $itc['codinope'],
                'CucuPers' => $itc['cucupers'],
                'CodiItco' => $itc['codiitno'],
                'MarcaItco' => $itc['marcaitco'],
                'AlteItco' => $itc['alteitco'],
                'DetaItco' => $itc['detaitco'],
                'CaraItco' => $itc['caraitco'],
                'CodiInsu' => $itc['codiinsu'],
                'CantItco' => $itc['cantitco'],
                'ImunItco' => $itc['imunitco'],
                'AltaUsua' => $cotizacion['email'],
            );
            $this->db->insert('itemcotizacion', $dataitc);
        }

        //actualiza items
        foreach ($arrayitc_upd as $itcu) {
            $this->db->set('CantItco', $itcu['cantitco']);
            $this->db->set('ImunItco', $itcu['imunitco']);
            $this->db->set('CaraItco', $itcu['caraitco']);
            $this->db->set('MarcaItco', $itcu['marcaitco']);
            $this->db->set('ModiUsua', $cotizacion['email']);
            $this->db->set('ModiFeho', $this->getHoraServidor());
            $this->db->where('PeriInfo', $itcu['periinfo']);
            $this->db->where('CodiNope', $itcu['codinope']);
            $this->db->where('CucuPers', $itcu['cucupers']);
            $this->db->where('CodiItco', $itcu['codiitno']);
            $this->db->where('AlteItco', $itcu['alteitco']);
            $this->db->update('itemcotizacion'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        }

        if ($this->db->trans_status() == false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateCoti($dtform = array(), $arrayitc = null, $arrayitc_upd = null) {

        $this->db->trans_begin();

        $this->db->set('PlenCoti', $dtform['plencoti']);
        $this->db->set('ObseCoti', $dtform['obsecoti']);
        $this->db->set('FemaofCoti', $dtform['femaofcoti']);
        $this->db->set('ModiUsua', $dtform['email']);
        $this->db->set('ModiFeho', $this->getHoraServidor());

        $this->db->where('PeriInfo', $dtform['periinfo']);
        $this->db->where('CodiNope', $dtform['codinope']);
        $this->db->where('CucuPers', $dtform['cucupers']);
        $this->db->update('cotizacion');


        foreach ($arrayitc as $itc) {
            $dataitc = array(
                'PeriInfo' => $itc['periinfo'],
                'CodiNope' => $itc['codinope'],
                'CucuPers' => $itc['cucupers'],
                'CodiItco' => $itc['codiitno'],
                'MarcaItco' => $itc['marcaitco'],
                'AlteItco' => $itc['alteitco'],
                'DetaItco' => $itc['detaitco'],
                'CaraItco' => $itc['caraitco'],
                'CodiInsu' => $itc['codiinsu'],
                'CantItco' => $itc['cantitco'],
                'ImunItco' => $itc['imunitco'],
                'AltaUsua' => $dtform['email'],
            );
            $this->db->insert('itemcotizacion', $dataitc);
        }
        //actualiza items
        foreach ($arrayitc_upd as $itcu) {
            $this->db->set('CantItco', $itcu['cantitco']);
            $this->db->set('ImunItco', $itcu['imunitco']);
            $this->db->set('CaraItco', $itcu['caraitco']);
            $this->db->set('MarcaItco', $itcu['marcaitco']);
            $this->db->set('ModiUsua', $dtform['email']);
            $this->db->set('ModiFeho', $this->getHoraServidor());
            $this->db->where('PeriInfo', $itcu['periinfo']);
            $this->db->where('CodiNope', $itcu['codinope']);
            $this->db->where('CucuPers', $itcu['cucupers']);
            $this->db->where('CodiItco', $itcu['codiitno']);
            $this->db->where('AlteItco', $itcu['alteitco']);
            $this->db->update('itemcotizacion'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        }
        if ($this->db->trans_status() == false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function find($cuit = NULL, $notape = null) {
        if ($cuit != null && $notape != null) {
            $this->reconectar();
            $this->db->select('*');
            $this->db->where('PeriInfo', $notape->PeriInfo);
            $this->db->where('CodiNope', $notape->CodiNope);
            $this->db->where('CucuPers', $cuit);
            $query = $this->db->get('cotizacion');
//            $query = $this->db->query("select * from cotizacion where PeriInfo = " . $notape->PeriInfo. " and CodiNope = " . $notape->CodiNope . " and CucuPers = " . $cuit . ";");
            
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

    public function existe($cotizacion = null) {
        if ($cotizacion != null) {
            $this->db->select('*');
            $this->db->where('PeriInfo', $cotizacion['periinfo']);
            $this->db->where('CodiNope', $cotizacion['codinope']);
            $this->db->where('CucuPers', $cotizacion['cucupers']);
            $query = $this->db->get('cotizacion');

//            $query = $this->db->query("select * from cotizacion where PeriInfo = " . $notape->PeriInfo 
//                    . " and CodiNope = " . $notape->CodiNope . " and CucuPers = " . $prov->CucuPers . ";");
            if ($query->num_rows() > 0) {
                $query->free_result();
                return true;
            } else {
                $query->free_result();
                return false;
            }
        }
    }

    public function delete($array = array()) {
        $this->db->where('PeriInfo', $array['periinfo']);
        $this->db->where('CodiNope', $array['codinope']);
        $this->db->where('CucuPers', $array['cucupers']);
        $this->db->delete('cotizacion');
    }

    
    public function getByRubrosXCuil($cuil = 0, $dataquery = array()) {
        if(empty($dataquery) || empty($dataquery['periodo'])){
            $dataquery['periodo'] = date('Y');
        }
        
        $this->reconectar();
        //$datos = array($cuil, $dataquery['periodo']);
        $query = $this->db->query("CALL `fnCotixProv`('".$cuil."', '".$dataquery['periodo']."')");
        $array = $query->result_array();
        $query->free_result();
        $areturn = array();
        
        if(!empty($dataquery['detanota'])){$dataquery['detanota'] = strtoupper($dataquery['detanota']);}
        if(!empty($dataquery['detarubro'])){$dataquery['detarubro'] = strtoupper($dataquery['detarubro']);}
        
        if(!empty($dataquery['detanota']) && !empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaNope']), $dataquery['detanota']) !== false &&
                        strpos(strtoupper($a['DetaRubro']), $dataquery['detarubro']) !== false){
                    array_push($areturn, $a);
                }
            }
        }else if(!empty($dataquery['detanota']) && empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaNope']), $dataquery['detanota']) !== false){
                    array_push($areturn, $a);
                }
            }
        }else if(empty($dataquery['detanota']) &&  !empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaRubro']), $dataquery['detarubro']) !== false){
                    array_push($areturn, $a);
                }
            }
        }else{
            return $array;
        }
        
        return $areturn;
    }

    public function getHoraServidor(){
        $query = $this->db->query("SELECT CURRENT_TIMESTAMP() AS fecha;");
        $array = $query->row_array();
        return $array['fecha'];
    }
    
    private function reconectar() {
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
    }
    

}
