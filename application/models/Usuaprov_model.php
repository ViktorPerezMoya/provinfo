<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuaprov
 *
 * @author VICTOR
 */
class Usuaprov_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function findUser($username = null, $password = null) {
        if ($username != null && $password != NULL) {
            $query1 = $this->db->query("SELECT infogov.fnValidarUsuaprov('" . $username."','".$password."') as validar");
            $row1 = $query1->row_array();
            $query1->free_result();
            
            if(trim($row1['validar']) == ""){
                
                $query2 = $this->db->query("SELECT IdUspro,UsuaUspro,ClavUspro,UlseUspro FROM usuaprov"
                        . " WHERE UsuaUspro = '" . $username."' AND ClavUspro = '".md5($password)."' LIMIT 1");
                $row2 = $query2->row();
                $query2->free_result();
                return $row2;
            }else{
                return $row1['validar'];
            }
        }
        return null;
    }
    
    public function findUserByEmail($email = null){
        if ($email != null) {
            $query = $this->db->query("SELECT IdUspro,UsuaUspro,ClavUspro,UlseUspro FROM usuaprov"
                    . " WHERE UsuaUspro = '" . $email."' LIMIT 1");
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return false;
    }
    
    public function updateUser($user) {
        $data = array(
            'ClavUspro' => $user->ClavUspro,
        );

        $this->db->where('IdUspro', $user->IdUspro);

        return $this->db->update('usuaprov', $data);
    }
    
    public function updateUltiSesi($user) {
        $data = array(
            'UlseUspro' => $user->UlseUspro,
        );

        $this->db->where('IdUspro', $user->IdUspro);

        return $this->db->update('usuaprov', $data);
    }
    
    public function getByCuil($cuil = null){
        if(!empty($cuil)){
        
            $query = $this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT(pxr.CucuPers,' - ', DetaProv)) sr_sroveedor , up.UsuaUspro su_usuario"
                .", up.ClaveTxtUspro primer_clave"
                ." FROM contpres.provxrubro pxr"
                ." INNER JOIN usuaprovcuit upc ON upc.CucuProv = pxr.CucuPers"
                ." INNER JOIN usuaprov up ON up.IdUspro = upc.IdUspro"
                ." INNER JOIN proveedor p ON p.CucuPers = pxr.CucuPers"
                ." WHERE pxr.CucuPers = $cuil AND up.PrimerCorreo IS NULL"
                ." GROUP BY up.IdUspro;");
            $rows = $query->result_array();
            $query->free_result();
            return $rows;
        }
    }
    
    public function getAll(){
        /*$this->db->select('IdUspro,UsuaUspro,ClavUspro,ClavTxtUspro');
        $query = $this->db->get('usuaprov');*/
        $query = $this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT(upc.CucuProv,' - ', DetaProv)) sr_sroveedor,"
                . " up.UsuaUspro su_usuario, up.ClaveTxtUspro primer_clave"
                ." FROM usuaprov up"
                ." INNER JOIN usuaprovcuit upc ON upc.IdUspro = up.IdUspro"
                ." INNER JOIN proveedor p ON p.CucuPers = upc.CucuProv"
                ." WHERE PrimerCorreo IS NULL"
                ." GROUP BY up.IdUspro"
                ." LIMIT 150;");
        $rows = $query->result_array();
        $query->free_result();
        return $rows;
    }
    
    public function notificado($email){
        $data = array(
            'PrimerCorreo' => $this->getHoraServidor()
        );
        $this->db->where('UsuaUspro', $email);
        return $this->db->update('usuaprov', $data);
    }
    
    
    public function getHoraServidor(){
        $query = $this->db->query("SELECT CURRENT_TIMESTAMP() AS fecha;");
        $array = $query->row_array();
        return $array['fecha'];
    }
    // jojo
    // punata
}
