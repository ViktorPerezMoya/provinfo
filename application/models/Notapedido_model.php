<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * SELECT
  `PeriInfo`,
  `CodiNope`,
  `CoinNope`,
  `CodiOrga`,
  `DetaNope`,
  `CodiTico`,
  `ExpeImpu`,
  `FechNope`,
  `FeliNope`,
  `HoliNope`,
  `FeleNope`,
  `HoleNope`,
  `FeanNope`,
  `MoanNope`,
  `FeocNope`,
  `PaesNope`,
  `AltaUsua`,
  `AltaFeho`,
  `ModiUsua`,
  `ModiFeho`,
  `BajaUsua`,
  `BajaFeho`,
  `BajaMoti`
  FROM `contpres_prueba`.`notapedido`
  LIMIT 0, 1000;
 */

/**
 * Description of Notapedido
 *
 * @author VICTOR
 */
class Notapedido_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getByRubro($id_rubro = 0) {
        if ($id_rubro > 0) {
            $query = $this->db->query("SELECT * FROM `notapedido` WHERE `IdRubro` = $id_rubro ORDER BY `PeriInfo` DESC");
            $result = $query->result();
            $query->free_result();
            return $result;
        }
        return null;
    }

    public function getByCodiNope($PeriInfo = 0, $CodiNope = 0) {
        if ($CodiNope > 0) {

//            if (mysqli_more_results($this->db->conn_id)) {
//                mysqli_next_result($this->db->conn_id);
//            }
            $query = $this->db->query("SELECT np.*,IF(np.IdUbfi > 0,"
                    . "(SELECT uf.DetaUbfi FROM ubicfisica uf WHERE uf.IdUbfi = np.IdUbfi),'') AS DetaUbfi FROM contpres.notapedido np "
                    . " WHERE np.PeriInfo = $PeriInfo AND np.CodiNope = $CodiNope ORDER BY np.PeriInfo DESC");
            $row =  $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

    
    public function getByRubrosXCuil($cuil = 0, $dataquery = array()) {
        if(empty($dataquery) || empty($dataquery['periodo'])){
            $dataquery['periodo'] = date('Y');
        }
        
        $this->reconectar();
        $datos = array($cuil, $dataquery['periodo']);
        $query = $this->db->query("CALL fnNotaxProv(?,?);", $datos);
        $array = $query->result_array();
        $query->free_result();
        $areturn = array();
        
        if(!empty($dataquery['detanota'])){$dataquery['detanota'] = strtoupper($dataquery['detanota']);}
        if(!empty($dataquery['detarubro'])){$dataquery['detarubro'] = strtoupper($dataquery['detarubro']);}
        
        if(!empty($dataquery['detanota']) && !empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaNope']), $dataquery['detanota']) !== false &&
                        strpos(strtoupper($a['DetaRubro']), $dataquery['detarubro'])  !== false ){
                    array_push($areturn, $a);
                }
            }
        }else if(!empty($dataquery['detanota']) && empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaNope']), $dataquery['detanota']) !== false){
                    array_push($areturn, $a);
                }
            }
        }else if(empty($dataquery['detanota']) &&  !empty($dataquery['detarubro'])){
            foreach ($array as $a){
                if(strpos(strtoupper($a['DetaRubro']), $dataquery['detarubro']) !== false){
                    array_push($areturn, $a);
                }
            }
        }else{
            return $array;
        }
        
        return $areturn;
    }
    private function reconectar() {
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
    }
    

}
