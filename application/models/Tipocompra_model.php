<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * SELECT CodiTico, DetaTico, LimiTico FROM tipocompra WHERE BajaFeho IS NULL;
  SELECT CodiTico, DetaTico, LimiTico from tipocompra where BajaFeho is null and CodiTico = 2;
 */

/**
 * Description of Tipocompra
 *
 * @author VICTOR
 */
class Tipocompra_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAll() {
        $query = $this->db->query("SELECT CodiTico, DetaTico, LimiTico FROM tipocompra WHERE BajaFeho IS NULL");
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    public function getByCodiTico($coditico = 0) {
        if ($coditico > 0) {
            $query = $this->db->query("SELECT CodiTico, DetaTico, LimiTico FROM tipocompra WHERE BajaFeho IS NULL and CodiTico = $coditico");
            $row = $query->row();
            $query->free_result();
            return $row;
        }
        return null;
    }

}
