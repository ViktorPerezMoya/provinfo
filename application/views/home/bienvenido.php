<div id="page-wrapper" style="padding-top: 10px;">
        <?php 
            if(!empty($breadcrumb)){
                    ?>

                <ol class="breadcrumb">
                    <?php 
                    foreach ($breadcrumb as $key=>$val):
                        if($val != ''){
                            ?>
                            <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                            <?php
                        }else{
                            ?>
                            <li class="active"><?=$key?></li>
                            <?php
                        }
                    endforeach;
                    ?>
                    
                  </ol>

                    <?php
            }
        ?>
    
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Bienvenido <small><?php echo $nameprov; ?></small></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
            <div class="row">
                <?php 
                $ultisesi = $this->session->userdata('ultimasesion');
                $row = get_count_news_rubros($ultisesi);
                if($row['cant'] > 0):
                    ?>
                        <div class="col-lg-3 col-md-6 offset-lg-3">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-file fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?= $row['cant'] ?></div>
                                            <div>Nuevos Rubros!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo base_url('index.php/rubros/') ?>">
                                    <div class="panel-footer">
                                        <span class="pull-left">Ver Rubros</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php
                endif;
                ?>
                
                
                <?php 
                $cuil = $this->session->userdata('cuilprov');
                $row = get_count_news_notas($ultisesi,$cuil);
                if($row['cant'] > 0):
                    ?>
                    <div class="col-lg-4 col-md-6  offset-lg-2">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?= $row['cant'] ?></div>
                                        <div>Nuevas Notas de Pedido!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('index.php/notaspedido/'); ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver Notas de Pedido</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                endif;
                ?>
                
                <?php 
                $cuil = $this->session->userdata('cuilprov');
                $row = get_count_news_facturas($ultisesi,$cuil);
                if($row['cant'] > 0):
                    ?>
                    <div class="col-lg-4 col-md-6  offset-lg-2">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-dollar fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?= $row['cant'] ?></div>
                                        <div>Nuevos Pagos!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('index.php/facturas/'); ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver Mis Facturas</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                endif;
                ?>
                
            </div>
    <div class="row img-responsive">
        <center>
            <img class="img img-responsive" style="width: 500px;margin-top: 50px;" src="<?php echo base_url() . 'public/img/logo_maipu.png' ?>"/>
        </center>
    </div>
</div>

