
<div class="container" style="padding-top: 10px;">
        <?php 
            if(!empty($breadcrumb)){
                    ?>

                <ol class="breadcrumb">
                    <?php 
                    foreach ($breadcrumb as $key=>$val):
                        if($val != ''){
                            ?>
                            <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                            <?php
                        }else{
                            ?>
                            <li class="active"><?=$key?></li>
                            <?php
                        }
                    endforeach;
                    ?>
                    
                  </ol>

                    <?php
            }
        ?>
    <?php if (sizeof($provs) > 1) { ?>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Tus entidades</h1>
                <p class="lead">Elije la entidad con la que deseas operar.</p>
            </div>
        </div>
    <?php } ?>
    <div class="row" style="margin-bottom: 100px;">

        <?php
        $tamanio = sizeof($provs);
        $colmd = "col-md-12";
        switch ($tamanio){
            case 1:
                $colmd = "col-md-12";
                break;
            case 2:
                $colmd = "col-md-6";
                break;
            case 3:
                $colmd = "col-md-4";
                break;
            default:
                $colmd = "col-md-6";
                break;
        }
        foreach ($provs as $p) {
            ?>
            <div class="<?= $colmd ?> proveedores_padre">
                <div class=" img-responsive proveedores">
                <a href="<?= base_url('index.php/home/bienvenido/'.$p->CucuPers); ?>"><img src="<?= base_url('public/img/proveedor_icon.png');?>" class="img"></a>
                </div>
                <a href="<?= base_url('index.php/home/bienvenido/'.$p->CucuPers); ?>" style="text-decoration: none;text-align: center;"><h4><?php echo $p->DetaProv; ?></h4></a>
            </div>
            <?php
        }
        ?>
    </div>

</div>

