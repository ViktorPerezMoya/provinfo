<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $title_head; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow" />
        <meta name="googlebot" content="index" />
        <meta name="description" content="Portal del Proveedor Maipú, aqui podras realizar tus operaciones como proveedore de la Municipalidad de Mapú.">
        <meta name="keywords" content="Portal del proveedor,Ordenes de compra,Notas de pedido, Rubros,Cotizaciones,Proveedores,Mendoza,Argentina,Orden,de,compra,notes,pedido,cotizacion,cotizaciones,Maipú,Municipalidad,Gestion,Expediente,Rubros,Categorias,Gutierrez,Coquinbito,Cruz de piera,Fray Luis Beltran,General Ortega,Las Barrancas,Lulunta,Luzuriaga,Rodeo del Medio,Russel,San Roque,2018,2017,2019,Guaymallen,Lavalle,Las Heras,Godoy Cruz,Ciudad,Tupungato,San Carlos,Tunuyan,Rivadavia,Santa Rosa,General Alvear,San Rafael,Malargüe,Malargue">
        <meta name="author" content="Infogov sa">

        <link rel="shortcut icon" href="<?=  base_url('/public/img/favicon.ico'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url().'public/css/estilos.css' ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <input type="hidden" value="Municipalidad de Maipú">
        <input type="hidden" value="Portal del proveedor">
        <input type="hidden" value="Mendoza">
        <div class="container">
            <div class="row align-items-center homepage">
                <div class="col-md-6 offset-md-3">
                    <img class="img img-responsive" src="<?php echo base_url() . 'public/img/logo_maipu.png' ?>" style="width: 350px; margin-bottom: 2rem;"/>
                    <h1>Portal del proveedor</h1>
                    <a class="btn btn-primary btn-lg" id="btn_link" href="<?php echo base_url('index.php/auth/login'); ?>">Identificarse</a>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $("#btn_link").focus();
        });
    </script>
</html>