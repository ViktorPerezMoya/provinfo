<div id="page-wrapper">
    <div class="row" style="padding-top: 10px;">
        <?php 
            if(!empty($breadcrumb)){
                    ?>

                <ol class="breadcrumb">
                    <?php 
                    foreach ($breadcrumb as $key=>$val):
                        if($val != ''){
                            ?>
                            <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                            <?php
                        }else{
                            ?>
                            <li class="active"><?=$key?></li>
                            <?php
                        }
                    endforeach;
                    ?>
                    
                  </ol>

                    <?php
            }
        ?>
        <div class="jumbotron" style="padding-left: 30px;">
            <h3>Configuración</h3>
        </div>
        <div class="col-md-4">
            <?php if (!empty($this->session->flashdata('success_settings'))) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p><?php echo $this->session->flashdata('success_settings'); ?></p>
                </div>
            <?php } ?>

            <?php if (!empty($this->session->flashdata('error_settings'))) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p><?php echo $this->session->flashdata('error_settings'); ?></p>
                </div>
            <?php } ?>

            <?php echo form_open('index.php/profile/settings'); ?>

            <div class="form-group">
                <label for="emailuser">Email: </label>
                <input type="email" class="form-control" id="emailuser" name="emailuser" value="<?php echo $usuario->UsuaUspro; ?>" disabled="true" placeholder="Ingrese email">
                <?php echo form_error('emailuser'); ?>
            </div>
            <div class="form-group">
                <label for="passuser">Password:</label>
                <input type="password" class="form-control" id="passuser" name="passuser" value="<?php //echo $usuario->ClavUsua; ?>" placeholder="Password">
                <?php echo form_error('passuser') ?>
            </div>

            <div class="form-group">
                <label for="passuserrepeat">Repetir password: </label>
                <input type="password" class="form-control" id="passuserrepeat" name="passuserrepeat" value="<?php //echo $usuario->ClavUsua; ?>" placeholder="Reitir Password">
                <?php echo form_error('passuserrepeat') ?>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>