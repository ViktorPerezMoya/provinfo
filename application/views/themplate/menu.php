<!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('index.php/home/bienvenido'); ?>">Portal del Proveedor
                <?php
                echo '<span class="user_now">(';
                if($this->session->userdata('nameprov')){
                   echo ''.$this->session->userdata('nameprov') .' <span> - '.$this->session->userdata('email').'</span>';
                }
                echo ')</span>';
                ?>
                    </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <?php  if(!empty($info)): ?>
                <li>
                    <a id="link_ayuda" title="Ayuda" data-toggle="modal" data-target="#modalAyuda">
                        <i class="far fa-question-circle"></i> 
                    </a>
                </li>
                <?php endif; ?>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fas fa-users-cog"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="<?php echo base_url('index.php/home/proveedores'); ?>"><i class="fa fa-user fa-fw"></i> Cambiar Proveedor</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/profile/settings'); ?>"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('index.php/auth/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation"  id="sidebar">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo base_url('index.php/home/proveedores'); ?>"><i class="fa fa-user fa-fw"></i> Cambiar Proveedor</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/rubros/') ?>"><i class="fa fa-cogs fa-fw"></i> Mis Rubros
                            <?php 
                            $ultisesi = $this->session->userdata('ultimasesion');
                            $row = get_count_news_rubros($ultisesi);
                            if($this->session->userdata('show_rubros_new') && $row['cant'] > 0):
                                echo '<small style="color:red;">('.$row['cant'].') Nuevos rubros</small>';
                            endif;
                            ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/notaspedido/'); ?>"><i class="fa fa-table fa-fw"></i> Notas de Pedido
                            <?php 
                            $cuil = $this->session->userdata('cuilprov');
                            $row = get_count_news_notas($ultisesi,$cuil);
                            if($this->session->userdata('show_notape_new') && $row['cant'] > 0):
                                echo '<small style="color:red;">('.$row['cant'].') Nuevas notas</small>';
                            endif;
                            ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/cotizacion/listar'); ?>"><i class="fa fa-table fa-fw"></i> Mis Cotizaciones</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/ordenescompra/index'); ?>"><i class="fa fa-file-text-o fa-fw"></i> Mis Ordenes de Compra</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('index.php/facturas/index'); ?>"><i class="fa fa-file-text-o fa-fw"></i> Mis Facturas
                            <?php 
                            $cuil = $this->session->userdata('cuilprov');
                            $row = get_count_news_facturas($ultisesi,$cuil);
                            if($this->session->userdata('show_facturas_new') && $row['cant'] > 0):
                                echo '<small style="color:red;">('.$row['cant'].') Nuevos pagos</small>';
                            endif;
                            ?>
                            </a>
                        </li>
                        <?php 
                        
                        $email = $this->session->userdata('email');
                        if($email == "victor.ariel.perez@gmail.com" || $email == "mauromoreno@infogov.com.ar"){
                            ?>
                                <li>
                                    <a href="<?php echo base_url('index.php/mail/index'); ?>"><i class="fa fa-envelope fa-fw"></i> E-mails Masivos</a>
                                </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse  -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<script type="text/javascript">
$(document).ready(function () {
        

        var offset = $("#sidebar").offset();
        var topPadding = 50;
        if($(window).width() >= 768){
            $(window).scroll(function () {
                if($(window).scrollTop() >= 50){
                    if ($("#sidebar").height() < $(window).height() && $(window).scrollTop() > offset.top) { /* LINEA MODIFICADA POR ALEX PARA NO ANIMAR SI EL SIDEBAR ES MAYOR AL TAMANO DE PANTALLA */
                        $("#sidebar").stop().animate({
                            marginTop: $(window).scrollTop() - offset.top + topPadding
                        });
                    } 
                }else{
                    $("#sidebar").stop().animate({
                            marginTop: 51
                        });
                }

            });
        }
    });
</script>
