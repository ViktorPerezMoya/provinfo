
    </div>
    <!-- /#wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="modalAyuda" tabindex="-1" role="dialog" aria-labelledby="myModalAyudaLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalAyudaLabel">Ayuda</h4>
          </div>
          <div class="modal-body">
                <div><?=$info?></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Metis Menu Plugin JavaScript -->
     <?php 
        if(!empty($arrayjs)){
            foreach ($arrayjs as $js):
                ?>
                <script src="<?php echo base_url('public').'/js/'.$js; ?>"></script>
                <?php
            endforeach;
        }
        ?>
    
    <script src="<?php echo base_url('public');?>/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url('public');?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('public');?>/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('public');?>/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('public');?>/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
