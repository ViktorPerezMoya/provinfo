<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $title_head; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Portal del Proveedor Maipú">
        <meta name="keywords" content="Proveedores,Mendoza,Argentina,Orden,de,compra,notes,pedido,cotizacion,cotizaciones,Maipú,Municipalidad,Gestion,Expediente,Rubros,Categorias,Gutierrez,Coquinbito,Cruz de piera,Fray Luis Beltran,General Ortega,Las Barrancas,Lulunta,Luzuriaga,Rodeo del Medio,Russel,San Roque,2018,2017,2019,Guaymallen,Lavalle,Las Heras,Godoy Cruz,Ciudad,Tupungato,San Carlos,Tunuyan,Rivadavia,Santa Rosa,General Alvear,San Rafael,Malargüe,Malargue">
        <meta name="author" content="Infogov sa">
        
        <link rel="shortcut icon" href="<?=  base_url('/public/img/favicon.ico'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url() . 'public/css/menu.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'public/css/estilos.css' ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <!-- Custom Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="<?php echo base_url('public'); ?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="<?php echo base_url('public'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
    </head>
    <body>