<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Portal del Proveedor Maipú">
        <meta name="keywords" content="Proveedores,Mendoza,Argentina,Orden,de,compra,notes,pedido,cotizacion,cotizaciones,Maipú,Municipalidad,Gestion,Expediente,Rubros,Categorias,Gutierrez,Coquinbito,Cruz de piera,Fray Luis Beltran,General Ortega,Las Barrancas,Lulunta,Luzuriaga,Rodeo del Medio,Russel,San Roque,2018,2017,2019,Guaymallen,Lavalle,Las Heras,Godoy Cruz,Ciudad,Tupungato,San Carlos,Tunuyan,Rivadavia,Santa Rosa,General Alvear,San Rafael,Malargüe,Malargue">
        <meta name="author" content="Infogov sa">

        <title><?= $title_head; ?></title>
        <link rel="shortcut icon" href="<?=  base_url('/public/img/favicon.ico'); ?>" />

        <link rel="stylesheet" href="<?php echo base_url() . 'public/css/estilos.css' ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('public'); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url('public'); ?>/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="<?php echo base_url('public'); ?>/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url('public'); ?>/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('public'); ?>/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url('public'); ?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="<?php echo base_url() . 'public/css/menu.css' ?>">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?php echo base_url('public'); ?>/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url('public'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120726314-1"></script>
        <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-120726314-1');
        </script>


    </head>

    <body>
        <div id="wrapper">

