
        <!-- menu -->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url('public/img/logo_menu.png');?>" width="30" height="30" class="d-inline-block align-top" alt="">
                Inicio
            </a>
        </nav>
        <!-- pagina -->
        <div class="container-fluid">
            <div class="row align-items-center loginpage">
                <div class="col-sm-8 offset-sm-2 col-md-4 offset-md-4">
                    <h1>Iniciar Sesión</h1>
                    <p style="color: red"><?php echo $this->session->flashdata('error_login'); ?></p>
                    <?php echo form_open('index.php/auth/login') ?>
                    <div class="form-group">
                        <input type="email" class="form-control" id="emailuser" name="emailuser" placeholder="Ingrese email">
                        <?php echo form_error('emailuser'); ?>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passuser" name="passuser" placeholder="Password">
                        <?php echo form_error('passuser') ?>
                        <p style="color: orange;padding: 10px;" id="pmayus"><i class="fas fa-exclamation-triangle"> </i> Boq. Mayus activado!</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <?php echo form_close() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-4" style="text-align: center;">
                    <a href="<?php echo base_url('index.php/auth/recuperar_pass'); ?>">Recuperar contraseña</a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function (){
            var cont = 0;
            $("#pmayus").hide();
            
            $('#passuser').keypress(function(e) {
                var s = String.fromCharCode( e.which ); 
                if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                    $("#pmayus").show();
                } else{
                    $("#pmayus").hide();
                }
            }); 
        });
        </script>