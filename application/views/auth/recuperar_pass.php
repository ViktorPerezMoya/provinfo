
        <!-- menu -->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="<?= base_url('index.php/'); ?>">
                <img src="<?= base_url('public/img/logo_menu.png');?>" width="30" height="30" class="d-inline-block align-top" alt="">
                Inicio
            </a>
        </nav>
        <!-- pagina -->
        <div class="container-fluid">
            <?php if($this->session->flashdata('success_recovery')){ ?>
            <div class="row justify-content-md-center">
                <div class="col-md-10">
                    <div class="jumbotron jumbotron-fluid" style="background-color: #ccffcc;color: #006633;margin-top: 10px;">
                        <div class="container">
                            <h1 class="display-4">Éxito</h1>
                            <p class="lead"><?php echo $this->session->flashdata('success_recovery'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row align-items-center recoverypage">
                <div class="col-sm-8 offset-sm-2 col-md-4 offset-md-4">
                    <h1>Solicitud de recuperación de clave</h1>
                    <p style="color: red"><?php echo $this->session->flashdata('error_recovery'); ?></p>
                    <?php echo form_open(base_url('index.php/auth/recuperar_pass')); ?>
                    <div class="form-group">
                        <input type="email" class="form-control" id="emailuser" name="emailuser" placeholder="Ingrese email">
                        <?php echo form_error('emailuser'); ?>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>