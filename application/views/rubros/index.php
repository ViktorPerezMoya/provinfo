
<div id="page-wrapper">
    
    <?php 
        if(!empty($breadcrumb)){
                ?>

            <ol class="breadcrumb">
                <?php 
                foreach ($breadcrumb as $key=>$val):
                    if($val != ''){
                        ?>
                        <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                        <?php
                    }else{
                        ?>
                        <li class="active"><?=$key?></li>
                        <?php
                    }
                endforeach;
                ?>

              </ol>

                <?php
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <h4>Mis Rubros</h4>
            <table class="table" id="mis_rubros">
                <thead>
                    <tr>
                        <th scope="col">Rubro</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($rubrosprov as $rub):
                        ?>

                        <tr>
                            <td><?= $rub->DetaRubro ?></td>
                            <td style="text-align: right;">
                                <a id="btn_accion" href="<?php echo base_url('index.php/rubros/remove_rubro/' . $rub->IdRubro) ?>" class="btn btn-danger">Quitar</button> 
                            </td>
                        </tr> 
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>

    </div>
    <hr>
    <div class="row">
        
        
        
        <div class="col-md-12">
            <h4>Rubros Disponibles</h4>
            <?php 
            
            $ulseUspro = $this->session->userdata('ultimasesion');
            $fecha = date_create_from_format('Y-m-d H:i:s', $ulseUspro);
            $fecha = date_format($fecha, 'Y-m-d H:i:s');
            $contnuev = 0;
            foreach ($rubros as $n) {
                if ($fecha < $n->AltaFeho) {
                    $contnuev++;
                }
            }
            
            
            if ($contnuev > 0) {
                ?>
                <div class="alert alert-info text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p>Se han encontrado <?= $contnuev ?> rubros nuevas desde su ultima sesión. </p>
                </div>
                <?php
            }
            ?>
            <div class="col-md-5 col-md-offset-7 form-inline " style="margin-bottom: 20px;">
                <?= form_open('index.php/rubros/index'); ?>
                <div class="form-group">
                    <label for="detarubro">Rubro: </label>
                    <input type="text" class="form-control" id="detarubro" name="detarubro" <?php
                    if (isset($dataform)): echo 'value="' . $dataform['DetaRubro'] . '"';
                    endif;
                    ?> placeholder="Rubro...">
                           <?php echo form_error('detalle'); ?>
                </div>
                <button type="submit" id="buscar" class="btn btn-primary">Buscar</button>
                <?= form_close() ?>
            </div>
            <table class="table" id="rubros_disp">
                <thead>
                    <tr>
                        <th>Rubro</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagincont = 0;
                    $index = $inicio_muestra;
                    $pagincont2 = 0;
                    foreach ($rubros as $rub):
                        $registrado = false;
                        foreach ($rubrosprov as $mirub):
                            if ($rub->IdRubro == $mirub->IdRubro) {
                                $registrado = true;
                                break;
                            }
                        endforeach;
                        if (!$registrado) {
                            
                            if ($pagincont >= $inicio_muestra && $pagincont2 < $cant_muestra) {
                                ?>

                                <tr class="<?= ($fecha < $rub->AltaFeho) ? 'info' : '' ?>">
                                    <td><?= $rub->DetaRubro ?> <?= ($fecha < $rub->AltaFeho) ? '<small style="color:red;"> (Nuevo)</small>' : '' ?></td>
                                    <td style="text-align: right;">
                                        <a id="btn_accion" href="<?php echo base_url('index.php/rubros/add_rubro/' . $rub->IdRubro) ?>" class="btn btn-success">Agregar</a> 
                                    </td>
                                </tr> 
                                <?php
                                $index++;
                                $pagincont2++;
                            }
                            $pagincont++;
                        }
                    endforeach;
                    ?>
                </tbody>
            </table>
            
            <?php echo $this->pagination->create_links() ?>
        </div>
    </div>
</div>
<!-- Mostrando registros del 1 al 10 de un total de 1,167 registros -->
<script>
    $(document).ready(function(){
        $("#buscar").attr("disabled",true);
        
        $("#detarubro").keypress(function(){
            console.log($(this).val().length);
            if($(this).val().length < 3){
                $("#buscar").attr("disabled",true);
            }else{
                $("#buscar").removeAttr("disabled");
            }
        });
    });
</script>