
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<style type="text/css">
    .hiddenRow{
        padding: 0 !important;
    }
    .row_detalle > td{
        padding: 5px;
    }
</style>
<div id="page-wrapper">
    <?php 
        if(!empty($breadcrumb)){
                ?>

            <ol class="breadcrumb">
                <?php 
                foreach ($breadcrumb as $key=>$val):
                    if($val != ''){
                        ?>
                        <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                        <?php
                    }else{
                        ?>
                        <li class="active"><?=$key?></li>
                        <?php
                    }
                endforeach;
                ?>

              </ol>

                <?php
        }
    ?>
<?php echo $this->session->flashdata('mensaje'); ?>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mis Ordenes de Compra</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Panel de Búsquda<a data-toggle="collapse"  aria-expanded="true"  href="#panel_busqueda" class="link_panel_busqueda"><i class="fas fa-chevron-up"></i></a>
                        </h4>
                    </div>
                    <div class="panel-body panel-collapse collapse in" id="panel_busqueda">
                        <div class="row">
                            <?= form_open(base_url('index.php/ordenescompra/index')); ?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="detalle">Detalle: </label>
                                    <input type="text" class="form-control" id="detalle" name="detalle" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['detanota'] . '"';
                                    endif;
                                    ?> placeholder="Detalle">
                                           <?php echo form_error('detalle'); ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="periodo">Año: </label>
                                    <input type="text" class="form-control" id="periodo" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['periodo'] . '"';
                                    endif;
                                    ?> name="periodo" placeholder="Año" value="<?php echo date('Y')?>">
                                           <?php echo form_error('periodo'); ?>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 15px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-default">Limpiar</button>
                            </div>

                            <?= form_close() ?>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
</div>
<div class="table-responsive">
    <table class="table table-striped table-hover" id="tb_orden">
        <thead>
            <tr>
                <th>#</th>
                <th>Detalle</th>
                <th>Tipo de compra</th>
                <th>Of. Solicitante</th>
                <th>Expediente</th>
                <th>Fecha</th>
                <th>Estado</th>
                <th>Imprimir</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $pagincont = 0;
            $index = $inicio_muestra;
            $pagincont2 = 0;
            foreach ($ordenes as $o) {
                if ($pagincont >= $inicio_muestra && $pagincont2 < $cant_muestra) {
                    ?>
                    <tr>
                        <th><?= $o['CodiOrco'] ?></th>
                        <td style="width: 35%;"><?= $o['DetaOrco'] ?></td>
                        <td><?= $o['DetaTico'] ?></td>
                        <td style="width: 15%;"><?= $o['DetaOrga'] ?></td>
                        <td><?= $o['ExpeImpu'] ?></td>
                        <td><?= $o['FechOrco'] ?></td>
                        <td>
                            <?php
                            if(!empty($o['FeanOrco'])){
                                echo 'ANULADA';
                            }else{
                                echo '';
                            }
                            ?>
                        </td>
                        <td>
                            <a href="<?= base_url('index.php/ordenescompra/imprimir/' . $o['PeriInfo'] . '/' . $o['CodiOrco']); ?>" target="_blank" class="btn btn-warning" alt="Cotizar"><i class="fas fa-print"></i></a>
                        </td>
                    </tr>
                    <?php
                    $index++;
                    $pagincont2++;
                }
                $pagincont++;
            }
            ?>
        </tbody>
    </table>
    <?php echo $this->pagination->create_links() ?>
</div>
<script>

    $(document).ready(function () {
//        $('#tb_orden').DataTable({
//            "language": {
//                "lengthMenu": "Mostrar _MENU_ registros por página",
//                "sSearch": "Buscar:",
//                "zeroRecords": "No se encontraron resultados",
//                "info": "Mostrando registros del _START_ a _END_ de _TOTAL_",
//                "infoEmpty": "No hay registros disponibles",
//                "infoFiltered": "(Total de registros filtrados _TOTAL_ )",
//                "sPrevious": "Anterior",
//                "sNext": "Siguiente"
//            }
//        });
    });
</script>