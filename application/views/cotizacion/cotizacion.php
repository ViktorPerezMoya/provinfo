<div id="page-wrapper" id="app">
    <ol class="breadcrumb">
        <?php 
        foreach ($breadcrumb as $key=>$val):
            if($val != ''){
                ?>
                <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                <?php
            }else{
                ?>
                <li class="active"><?=$key?></li>
                <?php
            }
        endforeach;
        ?>
    </ol>
    <?php // echo form_open('cotizacion/cotizacion/' . $notape->PeriInfo . '/' . $notape->CodiNope . '/'); ?>
    <div class="alert alert-dismissible" role="alert" id="divmessage">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div id="message"></div>
      </div>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cotización</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos de Cotización
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="form_coti">
                                    <fieldset>
                                        <div class="form-group col-md-6">
                                            <input type="hidden"  value="<?= $notape->PeriInfo; ?>" id="periinfocoti"/>
                                            <input type="hidden"  value="<?= $notape->FecoNope; ?>" id="fechalimite"/>
                                            <label for="codicoti">Nota de pedido (Número)</label>
                                            <input type="number" class="form-control" id="codicoti" id="codicoti" name="codicoti" value="<?= $notape->CodiNope; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="detacoti">Nota</label>
                                            <div class="input-group">
                                            <input type="text" class="form-control" id="detacoti" name="detacoti" value="<?= $notape->DetaNope; ?>" disabled="">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btn_ver_detanota"><i class="fa fa-eye"></i>
                                                </button>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3  has-success">
                                            <label for="detaubfi">Ubicación municipal</label>
                                            <input type="text" size="5" class="form-control" id="detaubfi" name="detaubfi" placeholder="" value="<?= $notape->DetaUbfi; ?>" disabled="">
                                            <label for="diennope">Dirección de entrega</label>
                                            <input type="text" class="form-control" id="diennope" name="diennope"  value="<?= $notape->DienNope; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-6  has-success">
                                            <label for="cuitprov">Cuit del Proveedor</label>
                                            <input type="number" size="5" class="form-control" id="cuitprov" name="cuitprov" placeholder="0000" value="<?= $prov->CucuPers; ?>" disabled="">
                                            <label for="detaprov">Razon social</label>
                                            <input type="text" class="form-control" id="detaprov" name="detaprov"  value="<?= $prov->DetaProv; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-3 has-success">
                                            <label for="coditico">Codigo de tipo de compra</label>
                                            <input type="number" class="form-control" id="codtico" name="coditico" placeholder="0000" value="<?= $tipocomp->CodiTico; ?>" disabled="">
                                            <label for="coditico">Tipo de compra</label>
                                            <input type="text" class="form-control" id="detatico" name="detatico" value="<?= $tipocomp->DetaTico; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="expecoti">Expediente</label>
                                            <input type="text" class="form-control" id="expecoti" name="expecoti" value="<?= $notape->ExpeImpu; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="fechacoti">Fecha</label>
                                            <input type="date" class="form-control" id="fechacoti" name="fechacoti"  value="<?= $notape->FechNope; ?>" disabled="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="fecocoti">Fecha de expiración:</label>
                                            <input type="datetime" class="form-control" id="fecocoti" name="fecocoti"  value="<?= $notape->FecoNope; ?>" disabled="">
                                        </div>
                                        </fieldset>
                                        <hr>
                                        <fieldset>
                                            <div class="form-group col-md-3">
                                                <label for="plencoti">Plazo de entrega *</label>
                                                <input type="text" class="form-control" id="plencoti" name="plencoti" maxlength="20" value="<?= (isset($coti->PlenCoti))?  $coti->PlenCoti : "" ?>" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="femaofcoti">Mantenimiento de la oferta hasta *</label>
                                                <input type="date" class="form-control" id="femaofcoti" name="femaofcoti" min="<?= $notape->FecoNope; ?>" value="<?= (isset( $coti->FemaofCoti))?  $coti->FemaofCoti : "" ?>" >
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group col-md-12">
                                                <label for="obsecoti">Observaciones</label>
                                                <input type="text" class="form-control" id="obsecoti" name="obsecoti" value="<?= (isset($coti->ObseCoti))? $coti->ObseCoti : "" ?>">
                                            </div>

                        <!--                    <div class="form-group  col-md-4">
                                              <label for="exampleInputFile">Subir archivo</label>
                                              <input type="file" id="exampleInputFile">
                                            </div>-->
                                        </fieldset>
                                        <fieldset style="text-align: center;">
                                            <a href="javascript:history.back(-1);" id="btncancelar" class="btn btn-primary">Cancelar</a>
                                            <input type="submit" value="Guardar cotización" class="btn btn-success"/>
                                            <?php if(isset($coti)):?>
                                            <a class="btn btn-danger" href="#" id="btn_eliminar_coti">Eliminar cotización</a>
                                            <?php endif;?>
                                        </fieldset>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
</div>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Atención: </strong>
            <ul>
                <li>Cotizar importes con <strong>I.V.A.</strong> incluido.</li>
                <li>Recuerde especificar la marca de cada Item.</li>
            </ul>
        </div>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col" id="th_codiitno">Item</th>
                <th scope="col" id="th_alteitco">Alte</th>
                <!--<th scope="col">Código</th>-->
                <th scope="col">Detalle de Insumo</th>
                <th scope="col">Detalle Ampliado</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Precio Unit.</th>
                <th scope="col">Total</th>
                <th scope="col">Opción</th>
            </tr>
        </thead>
        <tbody id="tabla_items">
            <?php
            $total_base = 0;
            $total_alte = 0;
            foreach ($itemsnota as $item) {
                ?>
                <tr class="fila">
                    <td id="tddelet<?= $item['PeriInfo'] . '_' . $item['CodiNope'] . '_' . $prov->CucuPers . '_' . $item['CodiItno']. '_' . $item['AlteItco']; ?>">
                        <?php if((float)$item['ImpoUnitItem'] > 0) { ?>
                        <a href="javascript:void(0);" id="link_del" onclick="borrarItemCotizacion(<?= $item['PeriInfo'] . ',' . $item['CodiNope'] . ',' . $prov->CucuPers . ',' . $item['CodiItno']. ',' . $item['AlteItco'].',\' '.$notape->FecoNope.'\''; ?>);" style="text-decoration: none; color: #ff6666;"><i class="fas fa-trash-alt"></i></a>
                        <?php }?>
                    </td>
                    <td scope="row" id="td_codiitno" class="td_codiitno">
                        <input type="hidden" name="periinfo" value="<?= $item['PeriInfo']; ?>"/>
                        <input type="hidden" name="codinope" value="<?= $item['CodiNope']; ?>"/>
                        <input type="hidden" name="cucupers" value="<?= $prov->CucuPers; ?>"/>
                        <input type="hidden" name="codiitno" value="<?= $item['CodiItno']; ?>"/>
                        <input type="hidden" name="codiinsu" value="<?= $item['CodiInsu']; ?>"/>
                        <input type="hidden" name="marcainsu" value="<?= $item['MarcaInsu']; ?>"/>
                        <input type="hidden" name="marcaitco" value="<?= $item['MarcaItco']; ?>"/>
                        <?= $item['CodiItno']; ?>
                    </td>
                    <td id="td_alteitco" class="td_alteitco">
                        <?php 
                        if(!empty($item['AlteItco'])){
                            echo trim($item['AlteItco']);
                        }else{
                            echo "0";
                        }
                        ?>
                    </td>
                    <!--<td id="td_codiinsu"><?php//echo $item['CodiInsu']; ?></td>-->
                    <td id="td_detaitem"><?= trim($item['DetaItem']); ?></td>
                    <td id="td_caractitem" style="width: 35%">
                        <div class="input-group">
                            <input type="text" class="form-control calcular inp_visualizar" name="detalleampli" value="<?= trim($item['CaractItem']); ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-default visualizar" type="button"><i class="fas fa-eye"></i></button>
                            </span>
                        </div>
                    </td>
                    <td id="tdcant" <?php if((float)$item['ImpoUnitItem'] > 0){ echo 'class="success"';} ?> style="width: 10%;">

                        <input type="float" name="cantidad" class="form-control flotante calcular_total" min="0" size="4"  max="<?= (int) trim($item['CantItem']); ?>" value="<?= (float) $item['CantItem']; ?>"/>

                    </td>
                    <td id="tdimpo" <?php if((float)$item['ImpoUnitItem'] > 0){ echo 'class="success"';}?> style="width: 15%;">

                            <input type="float" name="importeunitario" class="form-control flotante calcular_total" min="0" value="<?php echo (((float)$item['ImpoUnitItem'] > 0) ? (float)$item['ImpoUnitItem'] :"0.0"); ?>"/>

                    </td>
                    <td <?php if((float)$item['ImpoUnitItem'] > 0){ echo 'class="danger"';} ?> id="tdtotal" style="vertical-align: middle;">
                        <p class="total" style="margin-bottom: 0;">
                            <?= ($item['ImpoUnitItem'] > 0) ? ((float)$item['ImpoUnitItem'] * (float)$item['CantItem']) : "" ?>
                        </p>
                    </td>
                    <td>
                        <?php
                        if ($item['AlteItco'] == 0) {
                            $total_base += (float)$item['ImpoUnitItem'] * (float)$item['CantItem'];
                            ?>
                            <button class="btn btn-info btn-sm btnShowModal">+ Alternativo</button>
                            <?php
                        }else{
                            $total_alte += (float)$item['ImpoUnitItem'] * (float)$item['CantItem'];
                            ?>
                            <button class="btn btn-sm" disabled>+ Alternativo</button>    
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="6">
                </th>
                <th class="thfooter" colspan="3">
                    <p>Total Basicos: $ <span id="total_basi"><?= $total_base; ?></span></p>
                </th>
            </tr>
            <tr>
                <th colspan="6" >
                </th>
                <th class="thfooter" colspan="3">
                    <p>Total Alternativos: $ <span id="total_alte"><?= $total_alte; ?></span><p>
                </th>
            </tr>
        </tfoot>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="modalWarningLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalWarningLabel">¡Atención!!</h4>
            </div>
            <div class="modal-body">
                Usted esta por borrar una cotización.<br>
                ¿Estas seguro de continuar?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="supcotizacion">Sí</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalItemAlternativo" tabindex="-1" role="dialog" aria-labelledby="modalWarningLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo articulo alternativo.</h4>
            </div>
            <div class="modal-body">
                <h3 id="title_modal">0001 - Detalle Insumo</h3>
                <div class="form-group">
                  <label for="txtdetaampli">Detalle ampliado del insumo</label>
                  <input type="text" class="form-control" id="txtdetaampli_modal" placeholder="Detalle...">
                </div>
                <div class="form-group">
                  <label for="txtcant_modal">Cantidad:</label>
                  <input type="float" class="form-control" id="txtcant_modal" placeholder="0.0">
                </div>
                <div class="form-group">
                  <label for="txtprecunit_modal">Precio unitario:</label>
                  <input type="float" class="form-control" id="txtprecunit_modal" placeholder="0.0">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnadditem_modal">Agregar Item</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_notif" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo_notificacion" style="color: red;">ERROR!!</h4>
            </div>
            <div class="modal-body">
                <h4 id="mensajeError"></h4>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalAlertOK" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_notifOK" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo_notificacion" style="color: green;">Información!!</h4>
            </div>
            <div class="modal-body">
                <h4 id="mensajeOK"></h4>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalViewDeta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >Detalle Ampliado</h4>
            </div>
            <div class="modal-body"> 
                <label>Detalle Original:</label>
                <textarea id="detalle_ampliado" class="form-control" rows="5" style="margin-bottom: 10px;" disabled="true"></textarea>
                <label for="marca_item">Marca: </label>
                <input type="text" id="marca_item" class="form-control">
                <label>Detalle (Opcional):</label>
                <textarea id="mi_deta_ampli" class="form-control" rows="5" style="margin-bottom: 10px;"></textarea>
                <div style="text-align: right;">
                    <button id="save_deta_ampli" class="btn btn-primary" >Guardar</button>
                    <input type="button" id="cancel_deta_ampli" class="btn btn-default" value="Cancelar">
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetaNota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >Nota</h4>
            </div>
            <div class="modal-body"> 
                <p id="detalle_nota" style="padding: 20px 40px;text-align: justify;"></p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
            
</script>