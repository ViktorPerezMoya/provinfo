
<style type="text/css">
    .hiddenRow{
        padding: 0 !important;
    }
    .row_detalle > td{
        padding: 5px;
    }
</style>
<div id="page-wrapper">
    <?php
    if (!empty($breadcrumb)) {
        ?>

        <ol class="breadcrumb">
            <?php
            foreach ($breadcrumb as $key => $val):
                if ($val != '') {
                    ?>
                    <li><a href="<?= base_url($val); ?>"><?= $key ?></a></li>
                    <?php
                } else {
                    ?>
                    <li class="active"><?= $key ?></li>
                    <?php
                }
            endforeach;
            ?>

        </ol>

        <?php
    }
    ?>
    <?php echo $this->session->flashdata('mensaje'); ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mis Cotizaciones</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Panel de Búsquda<a data-toggle="collapse"  aria-expanded="true"  href="#panel_busqueda" class="link_panel_busqueda"><i class="fas fa-chevron-up"></i></a>
                        </h4>
                    </div>
                    <div class="panel-body panel-collapse collapse in" id="panel_busqueda">
                        <div class="row">
                            <?= form_open(base_url('index.php/cotizacion/listar')); ?>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="detalle">Detalle: </label>
                                    <input type="text" class="form-control" id="detalle" name="detalle" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['detanota'] . '"';
                                    endif;
                                    ?> placeholder="Detalle">
                                           <?php echo form_error('detalle'); ?>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="selectrubro">Rubro</label>
                                    <select class="form-control" id="selectrubro" name="selectrubro">
                                        <option value="0">Seleccione rubro...</option>
                                        <?php
                                        foreach ($misrubros as $r) {
                                            ?><option <?php
                                            if (isset($dataform['detarubro'])) {
                                                if ($dataform['detarubro'] == $r->DetaRubro) {
                                                    echo 'selected';
                                                }
                                            }
                                            ?> value="<?= $r->DetaRubro ?>"><?= $r->DetaRubro ?></option><?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="periodo">Año: </label>
                                    <input type="text" class="form-control" id="periodo" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['periodo'] . '"';
                                    endif;
                                    ?> name="periodo" placeholder="Año" value="<?php echo date('Y')?>">
                                           <?php echo form_error('periodo'); ?>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 15px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-default">Limpiar</button>
                            </div>

                            <?= form_close() ?>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
</div>
<div class="table-responsive">
    <table class="table" id="notas_pedido">
        <thead>
            <tr>
                <th></th>
                <th>Número</th>
                <th>Detalle</th>
                <th>Tipo de compra</th>
                <th>Expediente</th>
                <th>Of. Solicitante</th>
                <th>Fecha Limite</th>
                <th>Rubro</th>
                <th>Cotizar</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $pagincont = 0; //contara los registros q estamos evaluando 
            $index = $inicio_muestra;
            foreach ($notas as $n) {
                if ($pagincont >= $inicio_muestra && $pagincont < ($inicio_muestra+$cant_muestra)) {
                        ?>
                        <tr>
                            <th scope="row">
                                <button class="btn btn-primary" type="button" alt="Ver detalles" data-toggle="collapse" data-target="#detalle<?= $n['PeriInfo'] . '_' . $n['CodiNope'] ?>" aria-expanded="false" aria-controls="detalle<?= $n['PeriInfo'] . '_' . $n['CodiNope'] ?>">
                                    <i class="fas fa-caret-down"></i>
                                </button>

                            </th>
                            <td><?= $n['CodiNope'] ?></td>
                            <td style="width: 35%;"><?= $n['DetaNope'] ?></td>
                            <td style="width: 15%;"><?= (!empty($n['DetaTico'])) ? $n['DetaTico'] : '' ?></td>
                            <td><?= $n['ExpeImpu'] ?></td>
                            <td style="width: 15%;"><?= $n['oficina_solicitante'] ?></td>
                            <td style="width: 15%;"><?= (!empty($n['FecoNope'])) ? $n['FecoNope'] : '' ?></td>
                            <td style="width: 15%;"><?= $n['DetaRubro'] ?></td>
                            <td><a href="<?= base_url('index.php/cotizacion/cotizacion/' . $n['PeriInfo'] . '/' . $n['CodiNope']); ?>" class="btn btn-warning" alt="Cotizar"><i class="fas fa-hand-holding-usd"></i></a></td>
                        </tr>
                        <tr>
                            <td  colspan="9" class="hiddenRow">
                                <div class="collapse" id="detalle<?= $n['PeriInfo'] . '_' . $n['CodiNope'] ?>">
                                    <div class="alert alert-secondary" role="alert">
                                        <table>

                                            <?php
                                            foreach ($itemsnota[$index] as $item) {
                                                ?>
                                                <tr class="row_detalle">
                                                    <td><?= $item['CodiItno'] ?></td>
                                                    <td><span <?php
                                                        if ($item['ImpoUnitItem'] > 0) {
                                                            echo 'style="color: #0033cc;"';
                                                        }
                                                        ?>><?= $item['DetaItem']; ?></span></td>
                                                    <td><?= $item['CaractItem']; ?></td>
                                                    <td><strong> Cantidad: <?= (int) $item['CantItem']; ?></strong></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr> 
                        <?php
                        $index++;
                    }
                        $pagincont++;
                }
            ?>
        </tbody>
    </table>
    <?php echo $this->pagination->create_links() ?>
</div>