
<div id="page-wrapper">
    
     <div class="row" style="padding-top: 10px;">
        <?php 
            if(!empty($breadcrumb)){
                    ?>

                <ol class="breadcrumb">
                    <?php 
                    foreach ($breadcrumb as $key=>$val):
                        if($val != ''){
                            ?>
                            <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                            <?php
                        }else{
                            ?>
                            <li class="active"><?=$key?></li>
                            <?php
                        }
                    endforeach;
                    ?>
                    
                  </ol>

                    <?php
            }
        ?>
        <div class="jumbotron" style="padding-left: 30px;">
            <h3>Noticicacion de cuentas activas</h3>
            <a class="btn btn-primary " href="<?= base_url('index.php/mail/send_email');?>" id="btn_enviar" >Notificar</a>
        </div>

        <div class="col-md-12">
            <?php if (!empty($this->session->flashdata('success_settings'))) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p><?php echo $this->session->flashdata('success_settings'); ?></p>
                </div>
            <?php } ?>

            <?php if (!empty($this->session->flashdata('error_settings'))) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p><?php echo $this->session->flashdata('error_settings'); ?></p>
                </div>
            <?php } ?>

            
        </div>
     </div>
    <div class="row" id="enviando">
        <div style="padding-top: 50px;text-align: center;"><h1>Enviando...</h1></div>
        <div style="display: flex;justify-content: center;align-items: center;padding: 50px;">
            <div>
                <img src="<?=  base_url('public/img/loading_blue.gif');?>" style="height: 100px; width: 100px;">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
            $("#enviando").hide();
        $("#btn_enviar").click(function (){
            $("#enviando").show();
        });
    });
    
    </script>