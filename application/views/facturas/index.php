
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<style type="text/css">
    .hiddenRow{
        padding: 0 !important;
    }
    .row_detalle > td{
        padding: 5px;
    }
</style>
<div id="page-wrapper">
    <?php 
        if(!empty($breadcrumb)){
                ?>

            <ol class="breadcrumb">
                <?php 
                foreach ($breadcrumb as $key=>$val):
                    if($val != ''){
                        ?>
                        <li><a href="<?=base_url($val);?>"><?=$key?></a></li>
                        <?php
                    }else{
                        ?>
                        <li class="active"><?=$key?></li>
                        <?php
                    }
                endforeach;
                ?>

              </ol>

                <?php
        }
    ?>
<?php echo $this->session->flashdata('mensaje'); ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mis Facturas</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Panel de Búsquda<a data-toggle="collapse"  aria-expanded="true"  href="#panel_busqueda" class="link_panel_busqueda"><i class="fas fa-chevron-up"></i></a>
                        </h4>
                    </div>
                    <div class="panel-body panel-collapse collapse in" id="panel_busqueda">
                        <div class="row">
                            <?= form_open(base_url('index.php/facturas/index')); ?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="numefac">Nro. de Factura: </label>
                                    <input type="text" class="form-control" id="numefac" name="numefac" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['numefac'] . '"';
                                    endif;
                                    ?> placeholder="Nro de Factura">
                                           <?php echo form_error('numefac'); ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="periodo">Año: </label>
                                    <input type="text" class="form-control" id="periodo" <?php
                                    if (isset($dataform)): echo 'value="' . $dataform['periodo'] . '"';
                                    endif;
                                    ?> name="periodo" placeholder="Año" value="<?php echo date('Y')?>">
                                           <?php echo form_error('periodo'); ?>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 15px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-default">Limpiar</button>
                            </div>

                            <?= form_close() ?>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
</div>
<div class="table-responsive">
    <?php
    $ulseUspro = $this->session->userdata('ultimasesion');
    $fecha = date_create_from_format('Y-m-d H:i:s', $ulseUspro);
    $fecha = date_format($fecha, 'Y-m-d H:i:s');
    $contnuev = 0;
    foreach ($ordenes as $o) {
        if ($fecha < $o['fecha_hora_pagado']) {
            $contnuev++;
        }
    }

    if ($contnuev > 0) {
        ?>
        <div class="alert alert-info text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p>Se han encontrado <?= $contnuev ?> nuevos pagos desde su ultima sesión. </p>
        </div>
        <?php
    }
    ?>
    <table class="table table-striped table-hover" id="tb_orden">
        <thead>
            <tr>
            	<td>Año</td>
                <th>Tipo</th>
                <th>Sucursal</th>
                <th>Nro de Factura</th>
                <th>Devengado</th>
                <th>Mandado a pagar</th>
                <th>Pagado</th>
                <th>Expediente</th>
                <th>Nota de pedido</th>
                <th>O. de Compra</th>
                <th>O. de Pago</th>
                <th>Tot. Retenciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $pagincont = 0;
            $index = $inicio_muestra;
            $pagincont2 = 0;
            foreach ($ordenes as $o) {
                if ($pagincont >= $inicio_muestra && $pagincont2 < $cant_muestra) {
                    ?>
                    <tr class="<?= (!empty($o['fecha_hora_pagado']) && $fecha < $o['fecha_hora_pagado']) ? 'info' : '' ?>">
                        <td><?= $o['PeriInfo'] ?></td>
                        <td><?= $o['tipo_factura'] ?></td>
                        <td><?= $o['punto_venta'] ?></td>
                        <th><?= $o['nro_factura'] ?></th>
                        <td><?= $o['impo_devengado'] ?></td>
                        <td><?= $o['impo_mandadopagar'] ?></td>
                        <td><?= $o['impo_pagado'] ?></td>
                        <td><?= $o['expdte_pago'] ?></td>
                        <td><?= $o['nota_pedido'] ?></td>
                        <td><?= $o['orden_compra'] ?></td>
                        <td><?= $o['orden_pago'] ?></td>
                        <td><?= $o['Total_retenciones'] ?></td>
                    </tr>
                    <?php
                    $index++;
                    $pagincont2++;
                }
                $pagincont++;
            }
            ?>
        </tbody>
    </table>
    <?php echo $this->pagination->create_links() ?>
</div>
<script>

    $(document).ready(function () {
//        $('#tb_orden').DataTable({
//            "language": {
//                "lengthMenu": "Mostrar _MENU_ registros por página",
//                "sSearch": "Buscar:",
//                "zeroRecords": "No se encontraron resultados",
//                "info": "Mostrando registros del _START_ a _END_ de _TOTAL_",
//                "infoEmpty": "No hay registros disponibles",
//                "infoFiltered": "(Total de registros filtrados _TOTAL_ )",
//                "sPrevious": "Anterior",
//                "sNext": "Siguiente"
//            }
//        });
    });
</script>