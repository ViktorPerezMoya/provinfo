<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 
if(!function_exists('get_count_news_rubros'))
{
 function get_count_news_rubros($ultisesi='1900-01-01')
 {
        
        $sql ="select count(*) as cant from contpres.rubro where rubro.AltaFeho > '$ultisesi';";
        //asignamos a $ci el super objeto de codeigniter
        //$ci será como $this
        $ci =& get_instance();
        if (mysqli_more_results($ci->db->conn_id)) {
            mysqli_next_result($ci->db->conn_id);
        }
        $query = $ci->db->query($sql);
        return $query->row_array();
 
 }
}


if(!function_exists('get_count_news_notas'))
{
 function get_count_news_notas($ultisesi='1900-01-01',$cuil)
 {
        
        $sql = "SELECT count(n.CodiNope) as cant FROM notapedido n"
                ." INNER JOIN rubro r ON r.IdRubro = n.IdRubro"
                ." INNER JOIN provxrubro pr ON pr.IdRubro = r.IdRubro"
                ." WHERE pr.cucupers = '$cuil' AND n.AltaFeho > '$ultisesi' AND n.FecoNope >= CURRENT_DATE() AND n.CodiTico = 1  AND n.PubliNope = 1;";
                //. " WHERE notapedido.AltaFeho > '$ultisesi';";
        //asignamos a $ci el super objeto de codeigniter
        //$ci será como $this
        $ci =& get_instance();
        if (mysqli_more_results($ci->db->conn_id)) {
            mysqli_next_result($ci->db->conn_id);
        }
        $query = $ci->db->query($sql);
        return $query->row_array();
 
 }
}

 
if(!function_exists('get_count_news_facturas'))
{
 function get_count_news_facturas($ultisesi='1900-01-01',$cuit)
 {
        
        $sql ="SELECT COUNT(*) as cant FROM imputacion i WHERE CucuPers = $cuit AND NumeFact != '' AND (DeveImpu != 0 OR MapaImpu != 0 OR PagaImpu != 0) AND AjusImpu = 0 AND PagaImpu != 0 AND  AltaFeho > '$ultisesi';";
        //asignamos a $ci el super objeto de codeigniter
        //$ci será como $this
        $ci =& get_instance();
        if (mysqli_more_results($ci->db->conn_id)) {
            mysqli_next_result($ci->db->conn_id);
        }
        $query = $ci->db->query($sql);
        return $query->row_array();
 
 }
}

//end application/helpers/ayuda_helper.php