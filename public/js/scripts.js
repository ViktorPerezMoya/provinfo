$(document).ready(function(){
    $(".link_panel_busqueda").click(function(){
        var iconS = $(this).find("i").attr("class");
        //alert(iconS);
        //fas fa-chevron-down  
        switch (iconS) {
            case "fas fa-chevron-down":
                $(this).find("i").removeClass("fa-chevron-down");
                $(this).find("i").addClass("fa-chevron-up");
                break;
            case "fas fa-chevron-up":
                $(this).find("i").removeClass("fa-chevron-up");
                $(this).find("i").addClass("fa-chevron-down");
                break;
        }
    });
});