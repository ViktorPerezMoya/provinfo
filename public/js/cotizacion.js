
    var arrayitc = new Array();
    var arrayAll = new Array();
    var cambios = false;
    var filaActual = null; //lo usaremos para guardar las descripciones del proveedor
    //var URL = "http://localhost/provinfo/index.php/";
    var URL = "https://infogov.com.ar/proveedores/maipu/index.php/";
    
    
    var total_basi = 0;
    var total_alte = 0;
    var total_item_ant = 0;
    var total_item = 0;
    
    $(document).ready(function () {
        //Notification.requestPermission();
        total_basi = parseFloat($("#total_basi").text());
        total_alte = parseFloat($("#total_alte").text());
        
        var itemAlte = null;//obeto fila actual
        var alternativo = new Object();
        //console.log($("#fechalimite").val());
        ocultarTds();
        $("#divmessage").hide();
        $(".flotante").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && e.ctrlKey === true) || 
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     return;
            }

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        
        $(".btnShowModal").click(function(){
           itemAlte = $(this).parents("tr.fila");
           alternativo.periinfo = itemAlte.find("input[name=periinfo]").val();
           alternativo.codinope = itemAlte.find("input[name=codinope]").val();
           alternativo.cucupers = itemAlte.find("input[name=cucupers]").val();
           alternativo.codiitno = itemAlte.find("input[name=codiitno]").val();
           alternativo.codiinsu = itemAlte.find("input[name=codiinsu]").val();
           alternativo.marcainsu = itemAlte.find("input[name=marcainsu]").val();
           alternativo.marcaitco = itemAlte.find("input[name=marcaitco]").val();
           alternativo.detaitem = itemAlte.find("td[id=td_detaitem]").html();
           
           //buscamos el mayor alternativo de ese tipo de item
           var mayor = 0;
            $("tr.fila").each(function(i,e){
                //console.log("INDEX: "+i);
                var row = $(this).find("td:eq(1)");
                var row2 = $(this).find("td:eq(2)");
                var row_periinfo = row.find("input[name=periinfo]").val();
                var row_codinope = row.find("input[name=codinope]").val();
                var row_cucupers = row.find("input[name=cucupers]").val();
                var row_codiitno = row.find("input[name=codiitno]").val();
                var row_alteitco = parseInt(row2.html());
                
                if(
                    row_periinfo === alternativo.periinfo &&
                    row_codinope === alternativo.codinope &&
                    row_cucupers === alternativo.cucupers &&
                    row_codiitno === alternativo.codiitno 
                    ){
                    if(row_alteitco > mayor){
                        mayor = row_alteitco;
                    }
                }
                
            });
            alternativo.alteitco = mayor;
           
           alternativo.cantitem = itemAlte.find("input[name=cantidad]").val();
           alternativo.max = itemAlte.find("input[name=cantidad]").attr('max');
           $("#txtcant_modal").val(alternativo.cantitem);
            $("#title_modal").text(alternativo.codiinsu+" - "+alternativo.detaitem);
            $('#modalItemAlternativo').modal('show');
            
        });
        $("#btnadditem_modal").click(function(){
            alternativo.caractitem = $("#txtdetaampli_modal").val();
            alternativo.cantitem = $("#txtcant_modal").val();
            alternativo.impounititem = $("#txtprecunit_modal").val();
            
            var html_row ='<tr class="fila">'+
                        '<td id="tddelet'+alternativo.periinfo+'_'+alternativo.codinope+'_'+alternativo.cucupers+'_'+alternativo.codiitno+'">'+
                        '</td>'+
                        '<td scope="row" class="td_codiitno">'+
                            '<input type="hidden" name="periinfo" value="'+alternativo.periinfo+'">'+
                            '<input type="hidden" name="codinope" value="'+alternativo.codinope+'">'+
                            '<input type="hidden" name="cucupers" value="'+alternativo.cucupers+'">'+
                            '<input type="hidden" name="codiitno" value="'+alternativo.codiitno+'">'+
                            '<input type="hidden" name="codiinsu" value="'+alternativo.codiinsu+'">'+
                            '<input type="hidden" name="marcainsu" value="'+alternativo.marcainsu+'">'+
                            '<input type="hidden" name="marcaitco" value="'+alternativo.marcaitco+'">'+
                            alternativo.codiitno+
                        '</td>'+
                        '<td id="td_alteitco" class="td_alteitco">'+(parseInt(alternativo.alteitco)+1)+'</td>'+
                        //'<td id="td_codiinsu">'+alternativo.codiinsu+'</td>'+
                        '<td id="td_detaitem">'+alternativo.detaitem+'</td>'+
                        '<td id="td_caractitem">'+
                            '<div class="input-group">'+
                                    '<input type="text" class="form-control calcular inp_visualizar" name="detalleampli" value="'+alternativo.caractitem+'">'+
                                  '<span class="input-group-btn">'+
                                    '<button class="btn btn-default visualizar" type="button"><i class="fas fa-eye"></i></button>'+
                                '</span>'+
                            '</div>'+
                        '</td>'+
                        '<td id="tdcant">'+
                            '<input type="float" name="cantidad" class="form-control flotante" min="0" size="4" max="'+alternativo.max+'" value="'+alternativo.cantitem+'">'+
                        '</td>'+
                        '<td id="tdimpo">'+
                            '<input type="float" name="importeunitario" class="form-control flotante calcular" min="0" value="'+alternativo.impounititem+'">'+
                        '</td>'+
                        '<td id="tdtotal"  style="vertical-align: middle;"><p class="total" style="margin-bottom: 0;"></p></td>'+
                        '<td>'+
                        '<button class="btn btn-sm" disabled>+ Alternativo</button>'+
                        '</td>'+
                    '</tr>';
            $("#tabla_items").append(html_row);
            ocultarTds();
            
            $(".calcular, .calcular_total").focusin(function (){                
                total_item_ant = 0;
                var fila = $(this).parents("tr.fila");
                if($.trim(fila.find("[class=total]").text()) != ""){
                    total_item_ant = parseFloat(fila.find("[class=total]").text());
                }
            });
            
            $(".calcular, .calcular_total").keydown(function (){                
                total_item_ant = 0;
                var fila = $(this).parents("tr.fila");
                if($.trim(fila.find("[class=total]").text()) != ""){
                    total_item_ant = parseFloat(fila.find("[class=total]").text());
                }
            });
            
            $(".calcular, .calcular_total").focusout(function ()
            {
                total_item_ant = 0;
                var fila = $(this).parents("tr.fila");
                if($.trim(fila.find("[class=total]").text()) != ""){
                    total_item_ant = parseFloat(fila.find("[class=total]").text());
                }
                calcularTotal(this, true);
            });
            
            $(".calcular_total").keyup(function (e)
            {
                calcularTotal(this, false);
            });
        
            
            $('#modalItemAlternativo').modal('hide');
            
            var fila = $('#tddelet'+alternativo.periinfo+'_'+alternativo.codinope+'_'+alternativo.cucupers+'_'+alternativo.codiitno).parents("tr.fila");
            fila.find("input[name=importeunitario]").focus();
//            fila.find("input[name=importeunitario]").select();
            
            //eventos de edicion de detalle ampliado
            $(".inp_visualizar").focus(function(){
                filaActual = $(this).parents("tr.fila");
                var caracteristica = filaActual.find("input[name=detalleampli]").val();
                $("#detalle_ampliado").text(caracteristica);
                var text_mrkitco = filaActual.find("input[name=marcaitco]").val();
                $("#marca_item").val(text_mrkitco);
                $("#modalViewDeta").modal('show');
                $("#mi_deta_ampli").focus();
            });
            $(".visualizar").click(function(){
                filaActual = $(this).parents("tr.fila");
                var caracteristica = filaActual.find("input[name=detalleampli]").val();
                $("#detalle_ampliado").text(caracteristica);
                var text_mrkitco = filaActual.find("input[name=marcaitco]").val();
                $("#marca_item").val(text_mrkitco);
                $("#modalViewDeta").modal('show');
                $("#mi_deta_ampli").focus();
            });
        });
        
        
        $(".calcular, .calcular_total").focusin(function (){                
            total_item_ant = 0;
            var fila = $(this).parents("tr.fila");
            if($.trim(fila.find("[class=total]").text()) != ""){
                total_item_ant = parseFloat(fila.find("[class=total]").text());
            }
        });
        
        $(".calcular, .calcular_total").keydown(function (){                
            total_item_ant = 0;
            var fila = $(this).parents("tr.fila");
            if($.trim(fila.find("[class=total]").text()) != ""){
                total_item_ant = parseFloat(fila.find("[class=total]").text());
            }
        });

        $(".calcular, .calcular_total").focusout(function ()
        {
            total_item_ant = 0;
            var fila = $(this).parents("tr.fila");
            if($.trim(fila.find("[class=total]").text()) != ""){
                total_item_ant = parseFloat(fila.find("[class=total]").text());
            }
            calcularTotal(this, true);
        });

        
        $(".calcular_total").keyup(function (e)
        {
            calcularTotal(this, false);
        });
        //eventos de edicion de detalle ampliado
        $(".inp_visualizar").focus(function(){
            filaActual = $(this).parents("tr.fila");
            //tomo el total actual del item
            total_item_ant = 0;
            if($.trim(filaActual.find("[class=total]").text()) != ""){
                total_item_ant = parseFloat(filaActual.find("[class=total]").text());
            }
            
            var caracteristica = filaActual.find("input[name=detalleampli]").val();
            $("#detalle_ampliado").text(caracteristica);
            var text_mrkitco = filaActual.find("input[name=marcaitco]").val();
            $("#marca_item").val(text_mrkitco);
            $("#modalViewDeta").modal('show');
            $("#mi_deta_ampli").focus();
        });
        $(".visualizar").click(function(){
            filaActual = $(this).parents("tr.fila");
            //tomo el total actual del item
            total_item_ant = 0;
            if($.trim(filaActual.find("[class=total]").text()) != ""){
                total_item_ant = parseFloat(filaActual.find("[class=total]").text());
            }
            
            var caracteristica = filaActual.find("input[name=detalleampli]").val();
            $("#detalle_ampliado").text(caracteristica);
            var text_mrkitco = filaActual.find("input[name=marcaitco]").val();
            $("#marca_item").val(text_mrkitco);
            $("#modalViewDeta").modal('show');
            $("#mi_deta_ampli").focus();
        });
        //guardo el detalle ampliado
        $("#save_deta_ampli").click(function(){
            var text_origin = $("#detalle_ampliado").val();
            var text_my_description = $("#mi_deta_ampli").val();
            var valid_mrk = filaActual.find("input[name=marcainsu]").val();
            var text_marca = "";
            
            if($.trim($("#marca_item").val()) != ""){
                text_marca = $("#marca_item").val();
            }
            if(valid_mrk == "1" && text_marca == ""){  
                $("#modalViewDeta").modal('hide');              
                $("#mensajeError").text("Debe ingresar marca");
                $('#modalAlert').modal('show');
                return;
            }
            
            filaActual.find("input[name=marcaitco]").val(text_marca);
            filaActual.find("input[name=detalleampli]").val(text_origin+" "+$.trim(text_my_description));
            $("#mi_deta_ampli").val("");
            $("#marca_item").val("");
            calcularTotal(filaActual.find("input[name=cantidad]"), false);
            $("#modalViewDeta").modal('hide');
        });
        $("#cancel_deta_ampli").click(function(){
             $("#modalViewDeta").modal('hide');
        });
        
        
        
        $("#btn_ver_detanota").click(function(){
            var detalle = $("#detacoti").val();
            $("#detalle_nota").text(detalle);
            $("#modalDetaNota").modal('show');
        });
        
        $("#supcotizacion").click(function () {
            var periodo = $("#periinfocoti").val();
            var fechalimite = $("#fechalimite").val();
            //console.log("PERIODO: " + periodo);
            var codicoti = $("#codicoti").val();
            var cucupers = $("#cuitprov").val();

            var request = $.ajax({
                url: URL+"cotizacion/delcotizacion",
                method: "POST",
                data: {"PeriInfo": periodo, "CodiNope": codicoti, "CucuPers": cucupers,"fechalimite":fechalimite},
                dataType: "html"
            });

            request.done(function (msg) {
                if(msg == "ERROR EXPIRED"){
                    $("#mensajeError").text("La fecha de cotización ha expirado");
                    $('#modalAlert').modal('show');
                    return;
                }else if(msg == 'SESION EXPIRADA'){
                    $("#mensajeError").text("La sesión ha expirado");
                    $('#modalAlert').modal('show');
                    $('#modalAlert').on('hidden.bs.modal', function (e) {
                        location.reload();
                      });
                    return ;
                }
                window.location.href = URL+"notaspedido/";
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });

        });
        
        
        $("#form_coti").on('submit', function(e){
            e.preventDefault();

            if($("#plencoti").val() !== '' &&
                    $("#femaofcoti").val() !== '' &&
                    (arrayitc.length >= 1 ||
                    cambios)){
                //console.log("Se va a guardar");
                var cotizacion = new Object();
                cotizacion.periinfo = $("#periinfocoti").val();
                cotizacion.codinope = $("#codicoti").val();
                cotizacion.cucupers = $("#cuitprov").val();
                cotizacion.plencoti = $("#plencoti").val();
                cotizacion.obsecoti = $("#obsecoti").val();
                cotizacion.femaofcoti = $("#femaofcoti").val();

                //console.log(arrayitc);
                //console.log(cotizacion);
                var fechalimite = $("#fechalimite").val();

                var request = $.ajax({
                    url: URL+"cotizacion/cotizacion_back",
                    method: "POST",
                    data: {"cotizacion":cotizacion,"arrayitc":arrayitc,"fechalimite":fechalimite},
                    dataType: "html"
                });

                request.done(function (msg) {
                    //console.log(msg);
                    if(msg == 'INFORMAR'){

                        //$("#divmessage").show().removeClass("alert-danger").addClass("alert-success");
                        //$("#message").html("<center><h3>Cambios guardados.</h3></center>");
                        $("#mensajeOK").text("Cambios guardados!");
                        $('#modalAlertOK').modal('show');
                        $('#modalAlertOK').on('hidden.bs.modal', function (e) {
                            // do something...
                            location.reload();
                          });
                    }else if(msg == "SIN ITEMS"){
                        $("#mensajeError").text("No ha ingresado Items en la cotización.");
                        $('#modalAlert').modal('show');
                    }else if(msg == 'ERROR EXPIRED'){
                        $("#titulo_notificacion").css({"color":"red"});
                        $("#titulo_notificacion").text("ERROR!");
                        $("#mensajeError").text("La fecha de cotización ha expirado");
                        $('#modalAlert').modal('show');
                    }else if(msg == 'SESION EXPIRADA'){
                        $("#titulo_notificacion").css({"color":"red"});
                        $("#titulo_notificacion").text("ERROR!");
                        $("#mensajeError").text("La sesión ha expirado");
                        $('#modalAlert').modal('show');
                        $('#modalAlert').on('hidden.bs.modal', function (e) {
                            location.reload();
                          });
                    }else{
                        $("#titulo_notificacion").css({"color":"red"});
                        $("#titulo_notificacion").text("ERROR!");
                        $("#mensajeError").text("Hubo un fallo en la transacción: "+msg);
                        $('#modalAlert').modal('show');
                    }
                });
                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }else if(arrayitc.length == 0 || !cambios){
                $("#mensajeError").text("No hay cambios realizados!");
                $('#modalAlert').modal('show');
            }else{
                $("#divmessage").show().removeClass("alert-success").addClass("alert-danger");
                var errores = "<p>Errores:</p><ul>";
                if($("#plencoti").val() === ''){
                   $("label[for='plencoti']").css({"color":"red"});
                   errores += "<li>El campo 'Plazo de Entrega' no puede ser vacio</li>";
                }else{
                   $("label[for='plencoti']").css({"color":"#333"});
                }
                if($("#femaofcoti").val() === ''){
                   $("label[for='femaofcoti']").css({"color":"red"});
                   errores += "<li>El campo 'Fecha de mantenimiento de oferta' no puede ser vacio</li>";
                }else{
                   $("label[for='femaofcoti']").css({"color":"#333"});
               }
               
                errores += "</ul>";
                
                $("#message").html(errores);
            }
         });
         
         $("#femaofcoti").change(function(){
            /*var title = "Atención!";
            var more = {
                    icon: "http://localhost/provinfo/public/img/logo_maipu.png",
                    body: "La fehcha seleccionada debe ser mayor o igual a la fecha de expiración"
                };*/
            var min = $(this).attr("min").split(" ")[0];
            var sel = $(this).val();
            //console.log(min);
            //console.log(sel);
            if(min > sel){
                /*notificar(title, more);*/
                 
                $("#mensajeError").text("La fecha seleccionada debe ser mayor o igual a la fecha de expiración");
                $('#modalAlert').modal('show');
                $(this).val(null);
            }

         });
         
         $("#btn_eliminar_coti").click(function(){
             
            var fechalimite = $("#fechalimite").val();
            
             var request = $.ajax({
                url: URL+"cotizacion/delcotizacion",
                method: "POST",
                data: {"fechalimite":fechalimite},
                dataType: "html"
            });

            request.done(function (msg) {
                if(msg == "ERROR EXPIRED"){
                    $("#mensajeError").text("La fecha de cotización ha expirado");
                    $('#modalAlert').modal('show');
                    return;
                }else if(msg == 'SESION EXPIRADA'){
                    $("#mensajeError").text("La sesión ha expirado");
                    $('#modalAlert').modal('show');
                    $('#modalAlert').on('hidden.bs.modal', function (e) {
                        location.reload();
                      });
                    return ;
                }else{
                    $("#modalWarning").modal('show');
                }
            });


         });
         
         $("#plencoti").keyup(function(){
             if(!cambios){
                 cambios = true;
             }
         });
         
         $("#femaofcoti").change(function(){
             if(!cambios){
                 cambios = true;
             }
         });
         
         $("#obsecoti").keyup(function(){
             if(!cambios){
                 cambios = true;
             }
         });
    });
    function notificar(titulo, opciones){
        if(Notification) {

                if (Notification.permission == "granted"){

                    var n = new Notification(titulo, opciones);
                    setTimeout( function() { n.close() }, 5000);

                }

                else if(Notification.permission == "default") {
                    alert("Primero da los permisos de notificación");
                }

                else {
                    alert("Bloqueaste los permisos de notificación");
                }
            }

            else {
                alert("Tu navegador no es compatible con API Notification");
            }
    }
    function ocultarTds(){
//        alert("x-y: "+$(window).width()+" - "+$(window).height());
        if($(window).width() < 1000){
            $("#th_codiitno").hide();
            $("#th_alteitco").hide();
            $(".td_codiitno").hide();
            $(".td_alteitco").hide();
        }
        
    }
    function quitarDeArray(fila){
        var itemcompra = new Object();
        itemcompra.periinfo = fila.find("input[name=periinfo]").val();
        itemcompra.codinope = fila.find("input[name=codinope]").val();
        itemcompra.cucupers = fila.find("input[name=cucupers]").val();
        itemcompra.codiitno = fila.find("input[name=codiitno]").val();
        itemcompra.alteitco = $.trim(fila.find("td[id=td_alteitco]").text());
        for(var i = 0; i< arrayitc.length; i++){
            //console.log(arrayitc[i]);
            if(
                arrayitc[i].periinfo === itemcompra.periinfo &&
                arrayitc[i].codinope === itemcompra.codinope &&
                arrayitc[i].cucupers === itemcompra.cucupers &&
                arrayitc[i].codiitno === itemcompra.codiitno &&
                arrayitc[i].alteitco === itemcompra.alteitco 
                ){
                    //console.log("ELEMENTO BORRADO DLE ARRAY");
                    fila.find("[class=total]").text("0");
//                    fila.find("td[id=tdcant]").removeClass('success');
//                    fila.find("td[id=tdimpo]").removeClass('success');
//                    fila.find("td[id=tdtotal]").removeClass('danger');
                    arrayitc.splice(i,1);
                    break;
            }
        }
    }
    function calcularTotal(elementRow,isFocusOut){
            var fila = $(elementRow).parents("tr.fila");
            var importe = fila.find("input[name=importeunitario]").val();
            var cantidad = fila.find("input[name=cantidad]").val();
            var maxcant = fila.find("input[name=cantidad]").attr('max');
            
            if (parseFloat(importe) * parseFloat(cantidad) > 0 &&
                    parseFloat(maxcant) >= parseFloat(cantidad)                    
                ) {
            
                if(fila.find("input[name=marcainsu]").val() == "1" && fila.find("input[name=marcaitco]").val() == ""){
                    $("#titulo_notificacion").css({"color":"orange"});
                        $("#titulo_notificacion").text("ADVERTNECIA!");
                        $("#mensajeError").html("Debe ingresar marca en <b>Detalle Ampliado</b>. ");
                        $('#modalAlert').modal('show');
                        return;
                }
                total_item = parseFloat(importe) * parseFloat(cantidad);
                fila.find("[class=total]").text(parseFloat(importe) * parseFloat(cantidad));
                fila.find("td[id=tdcant]").addClass('success');
                fila.find("td[id=tdimpo]").addClass('success');
                fila.find("td[id=tdtotal]").addClass('danger');

                var periinfo = fila.find("input[name=periinfo]").val();
                var codinope = fila.find("input[name=codinope]").val();
                var cucupers = fila.find("input[name=cucupers]").val();
                var codiitno = fila.find("input[name=codiitno]").val();
                var marcaitco = fila.find("input[name=marcaitco]").val();
                var caractitco = fila.find("input[name=detalleampli]").val();//fila.find("td[id=td_caractitem]").html();
                var alteitco = fila.find("td[id=td_alteitco]").html();
                
                var cantitcoStr = fila.find("input[name=cantidad]").val();
                var cantitco = cantitcoStr.replace("\n","");
                cantitco = $.trim(cantitco);
                cantitco = parseFloat(cantitco);
                var imunitco = fila.find("input[name=importeunitario]").val();

                
                var itemcompra = new Object();
                itemcompra.periinfo = periinfo;
                itemcompra.codinope = codinope;
                itemcompra.cucupers = cucupers;
                itemcompra.codiitno = codiitno;
                itemcompra.marcaitco = marcaitco;
                itemcompra.alteitco = $.trim(alteitco);
                itemcompra.cantitco = cantitco;
                itemcompra.imunitco = imunitco;
                itemcompra.caraitco = $.trim(caractitco);
                var nuevoitem = true;
                for(var i = 0; i< arrayitc.length; i++){
                    //console.log(arrayitc[i]);
                    if(
                        arrayitc[i].periinfo === itemcompra.periinfo &&
                        arrayitc[i].codinope === itemcompra.codinope &&
                        arrayitc[i].cucupers === itemcompra.cucupers &&
                        arrayitc[i].codiitno === itemcompra.codiitno &&
                        arrayitc[i].alteitco === itemcompra.alteitco 
                        ){
                            //console.log("VALOR ENCONTRADO");
                            if(
                            arrayitc[i].caraitco !== itemcompra.caraitco ||
                            arrayitc[i].cantitco !== itemcompra.cantitco ||
                            arrayitc[i].imunitco !== itemcompra.imunitco  
                            ){//se cambio alguno de los valores de cantidad o precio unitario asi que actualizamos el array
                                //console.log("VALOR MODIFICADOOO");
                                arrayitc[i].caraitco = itemcompra.caraitco;
                                arrayitc[i].cantitco = itemcompra.cantitco;
                                arrayitc[i].imunitco = itemcompra.imunitco; 
                                arrayitc[i].marcaitco = itemcompra.marcaitco; 
                            }
                            nuevoitem = false;
                            
                            break;
                    }
                }
                if(nuevoitem){
                    arrayitc.push(itemcompra);
                    //console.log("Se agrego una fila");
                    cambios =  true;
                }
                
                /** ACTULIZAMOS LOS TOTALES **/
                if(itemcompra.alteitco > 0){
                    console.log("OPERACION ALT: ("+total_alte+" - "+total_item_ant+") + "+total_item);
                    total_alte = (total_alte - total_item_ant) + total_item;
                    $("#total_alte").text(total_alte);
                }else{
                    console.log("OPERACION BAS: ("+total_basi+" - "+total_item_ant+") + "+total_item);
                    total_basi = (total_basi - total_item_ant) + total_item;
                    $("#total_basi").text(total_basi);
                }
                
            }else{
                    if (parseInt(importe) * parseInt(cantidad) < 0) {
                        $("#titulo_notificacion").css({"color":"orange"});
                        $("#titulo_notificacion").text("ADVERTNECIA!");
                        $("#mensajeError").text("Ingrese sólo números positivos.");
                        $('#modalAlert').modal('show');
                        quitarDeArray(fila);
                    }else if (maxcant < cantidad) {
                        $("#titulo_notificacion").css({"color":"orange"});
                        $("#titulo_notificacion").text("ADVERTNECIA!");
                        $("#mensajeError").text("La cantidad no puede ser mayor a la demandada por el municipio.");
                        $('#modalAlert').modal('show');
                        quitarDeArray(fila);
                    }else if((importe === '' || cantidad === '' ) && isFocusOut){
                        $("#titulo_notificacion").css({"color":"orange"});
                        $("#titulo_notificacion").text("ADVERTNECIA!");
                        $("#mensajeError").text("Ha dejado un campo vacio");
                        $('#modalAlert').modal('show');
                        quitarDeArray(fila);

                    }
            }
    }
    
    function borrarItemCotizacion(periinfo, codinope, cucupers, codiitno,alteitco,fechalimite) {
        var nomtd = "#tddelet" + periinfo + "_" + codinope + "_" + cucupers + "_" + codiitno+ "_" + alteitco;

        var request = $.ajax({
            url: URL+"cotizacion/registrarcambio",
            method: "POST",
            data: {"PeriInfo": periinfo, "CodiNope": codinope, "CucuPers": cucupers, "CodiItno": codiitno, "AlteItco": alteitco, "fechalimite":fechalimite },
            dataType: "html"
        });

        request.done(function (msg) {
            //console.log(msg);
            if(msg == "ERROR EXPIRED"){
                $("#mensajeError").text("La fecha de cotización ha expirado");
                $('#modalAlert').modal('show');
                return;
            }else if(msg == 'SESION EXPIRADA'){
                $("#mensajeError").text("La sesión ha expirado");
                $('#modalAlert').modal('show');
                $('#modalAlert').on('hidden.bs.modal', function (e) {
                    location.reload();
                  });
            }else if(msg == 'COTIZACION DELETED'){
                $("#mensajeOK").text("Cotización borrada!");
                $('#modalAlertOK').modal('show');
                $('#modalAlertOK').on('hidden.bs.modal', function (e) {
                    location.reload();
                  });
            }else{
                $(nomtd).html("");
                $(nomtd).parents("tr").find("td[id=tdcant]").removeClass('success');
                $(nomtd).parents("tr").find("td[id=tdimpo]").removeClass('success');
                $(nomtd).parents("tr").find("td[id=tdtotal]").removeClass('danger');
                //$(nomtd).parents("tr").find("input[name=cantidad]").val("");
                $(nomtd).parents("tr").find("input[name=importeunitario]").val("0.0");
                $(nomtd).parents("tr").find("[class=total]").text("");
                var alte = parseInt($(nomtd).parents("tr").find("td[id=td_alteitco]").html());
                if(alte > 0){
                    $(nomtd).parents("tr").hide();
                }
            }
        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    }